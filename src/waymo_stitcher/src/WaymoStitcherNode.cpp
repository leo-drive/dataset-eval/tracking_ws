//
// Created by goktug on 6.04.2021.
//

#include "WaymoStitcherNode.h"

using namespace std::chrono_literals;

WaymoStitcherNode::WaymoStitcherNode()
:Node("waymo_stitcher_node"),
 id_msg_clouds(0),
 id_msg_lid_labels(0)
{
    pub_stitched_cloud_ = create_publisher<sensor_msgs::msg::PointCloud2>("/cloud_stitched", 10);

    sub_lid_front_.subscribe(this,"/cloud_front");
    sub_lid_right_.subscribe(this,"/cloud_right");
    sub_lid_rear_.subscribe(this,"/cloud_rear");
    sub_lid_left_.subscribe(this,"/cloud_left");
    sub_lid_top_.subscribe(this,"/cloud_top");

    functionSub_lid_labels_ = std::bind(&WaymoStitcherNode::CallbackSubLidarLabels, this, std::placeholders::_1);
    sub_lid_labels_ = this->create_subscription<visualization_msgs::msg::MarkerArray>(
            "/lidar_labels", 1, functionSub_lid_labels_);

    sync_ = std::make_shared<message_filters::TimeSynchronizer<
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2>>(
            sub_lid_front_,
            sub_lid_right_,
            sub_lid_rear_,
            sub_lid_left_,
            sub_lid_top_,12);

    sync_->registerCallback(std::bind(&WaymoStitcherNode::CallbackSync, this,
                                      std::placeholders::_1,
                                      std::placeholders::_2,
                                      std::placeholders::_3,
                                      std::placeholders::_4,
                                      std::placeholders::_5));



/*    tf_buffer_ = std::make_shared<tf2_ros::Buffer>(this->get_clock());
    tf_buffer_->setUsingDedicatedThread(true);
    tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_, this, false);*/
}

void
WaymoStitcherNode::CallbackSync(const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_front,
                              const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_right,
                              const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_rear,
                              const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_left,
                              const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_top)
{
/*    geometry_msgs::msg::TransformStamped trans_vehicle_to_global;
    try {
        trans_vehicle_to_global = tf_buffer_->lookupTransform("global", "vehicle", rclcpp::Time(0));
    }
    catch (tf2::TransformException &ex) {
        RCLCPP_WARN(this->get_logger(), "[Tf2ListenerLog]: '%s'", ex.what());
    }
    Eigen::Affine3d affine_vehicle_to_global = tf2::transformToEigen(trans_vehicle_to_global);
    Eigen::Matrix4d mat_vehicle_to_global = affine_vehicle_to_global.matrix();*/


    /* std::cout << "Vehicle pose in global coord x: " << mat_vehicle_to_global(0,3) << " y:"
     << mat_vehicle_to_global(1,3) << " z:" << mat_vehicle_to_global(2,3) << std::endl;*/

    Cloud::Ptr cloud_front(new Cloud);
    pcl::fromROSMsg(*msg_front,*cloud_front);
    Cloud::Ptr cloud_right(new Cloud);
    pcl::fromROSMsg(*msg_right,*cloud_right);
    Cloud::Ptr cloud_rear(new Cloud);
    pcl::fromROSMsg(*msg_rear,*cloud_rear);
    Cloud::Ptr cloud_left(new Cloud);
    pcl::fromROSMsg(*msg_left,*cloud_left);
    Cloud::Ptr cloud_top(new Cloud);
    pcl::fromROSMsg(*msg_top,*cloud_top);
    Cloud::Ptr cloud_stitched(new Cloud);
    *cloud_stitched += *cloud_front + *cloud_left + *cloud_rear + *cloud_right + *cloud_top;


    std::cout << "***********************************************" << std::endl;
    std::cout << "Msg cloud id: " << id_msg_clouds << std::endl;
    std::cout << "Current timestamp: " << msg_front->header.stamp.sec << "(sec)" << std::endl;
    std::cout << "Current timestamp: " << msg_front->header.stamp.nanosec << "(nanosec)" << std::endl;
    id_msg_clouds += 1;
    ROSRelated::PublishPointCloud(pub_stitched_cloud_, cloud_stitched, msg_front);
    std::cout << "Stitched cloud is published." << std::endl;
    std::cout << "***********************************************" << std::endl;
}

void
WaymoStitcherNode::CallbackSubLidarLabels(const visualization_msgs::msg::MarkerArray::ConstSharedPtr &msg_lidar_labels) {
    /*std::cout << "***********************************************" << std::endl;
    std::cout << "Msg label id: " << id_msg_lid_labels << std::endl;
    std::cout << "Current timestamp label: " << msg_lidar_labels->markers[0].header.stamp.sec << "(sec)" << std::endl;
    std::cout << "Current timestamp label: " << msg_lidar_labels->markers[0].header.stamp.nanosec << "(nanosec)" << std::endl;
    id_msg_lid_labels += 1;
    std::cout << "***********************************************" << std::endl;

    WaymoStitcherNode::LidarLabels lidarLabels;
    lidarLabels.msg_lidar_labels = msg_lidar_labels;
    lidarLabels.sec = msg_lidar_labels->markers[0].header.stamp.sec;
    lidarLabels.nsec = msg_lidar_labels->markers[0].header.stamp.nanosec;

    deque_lid_labels.push_back(lidarLabels);

    if(WaymoStitcherNode::deque_lid_labels.size() == 20)
        deque_lid_labels.pop_front();*/
}
