//
// Created by goktug on 14.04.2021.
//

#include "PCLStuff.h"

using Point = pcl::PointXYZ;
using Cloud = pcl::PointCloud<pcl::PointXYZ>;

using PointInt = pcl::PointXYZI;
using CloudInt = pcl::PointCloud<pcl::PointXYZI>;

Cloud::Ptr
PCLStuff::DownSample(const Cloud::Ptr &cloud, const float &leaf_size)
{
    Cloud::Ptr output(new Cloud);
    pcl::VoxelGrid<Point> sor;
    sor.setInputCloud(cloud);
    sor.setLeafSize(leaf_size,leaf_size,leaf_size);
    sor.filter(*output);
    //std::cout << "Down-sampling is done. Leaf size: " << leaf_size << std::endl;
    return output;
}

std::pair<std::vector<CloudInt::Ptr>,CloudInt::Ptr>
PCLStuff::Detect3D(const Cloud::Ptr& cloud)
{
    Cloud::Ptr cloud_ds = DownSample(cloud,0.4);
    auto pair = EuclideanClustering(cloud_ds,
                     0.99,5,10000);

    return pair;
}

std::pair<std::vector<CloudInt::Ptr>,CloudInt::Ptr>
PCLStuff::EuclideanClustering(const Cloud::Ptr& cloud,
                              const float& tolerance,
                              const int& min_point_count,
                              const int& max_point_count)
{
    pcl::search::KdTree<Point>::Ptr kdtree(
            new pcl::search::KdTree<Point>);
    kdtree->setInputCloud(cloud);

    std::vector<pcl::PointIndices> vec_cluster_indices;

    pcl::EuclideanClusterExtraction<Point> ec;
    ec.setClusterTolerance(tolerance);
    ec.setMinClusterSize(min_point_count);
    ec.setMaxClusterSize(max_point_count);
    ec.setSearchMethod(kdtree);
    ec.setInputCloud(cloud);
    ec.extract(vec_cluster_indices);

    std::vector<CloudInt::Ptr> clusters;
    CloudInt::Ptr cloud_centroids(new CloudInt);

    float intensity = 0;

    for(const auto& cluster_indices : vec_cluster_indices)
    {
        CloudInt::Ptr cluster(new CloudInt);
        for(const auto& index : cluster_indices.indices)
        {
            Point p = cloud->points[index];
            PointInt p_;
            p_.x = p.x;
            p_.y = p.y;
            p_.z = p.z;
            p_.intensity = intensity;
            cluster->points.push_back(p_);
        }
        clusters.push_back(cluster);
        Eigen::Vector4f centroid_eigen;
        pcl::compute3DCentroid(*cluster, centroid_eigen);
        PointInt centroid;
        centroid.x = centroid_eigen(0);
        centroid.y = centroid_eigen(1);
        centroid.z = centroid_eigen(2);
        cloud_centroids->points.push_back(centroid);
        intensity += 100;
    }
    std::pair<std::vector<CloudInt::Ptr>,CloudInt::Ptr> pair;
    pair.first = clusters;
    pair.second = cloud_centroids;
    return pair;
}
