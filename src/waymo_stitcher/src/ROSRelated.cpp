//
// Created by goktug on 14.04.2021.
//

#include "ROSRelated.h"


void ROSRelated::PublishPointCloud(const rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr& publisher,
                                   const Cloud::Ptr& cloud,
                                   const sensor_msgs::msg::PointCloud2::ConstSharedPtr & msg_point_cloud) {
    sensor_msgs::msg::PointCloud2 msg;
    pcl::toROSMsg(*cloud, msg);
    msg.header = msg_point_cloud->header;
    publisher->publish(msg);
}

