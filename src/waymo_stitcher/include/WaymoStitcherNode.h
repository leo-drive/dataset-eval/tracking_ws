//
// Created by goktug on 6.04.2021.
//

#ifndef BUILD_WAYMOSTITCHERNODE_H
#define BUILD_WAYMOSTITCHERNODE_H

#include "ROSRelated.h"
#include "PCLStuff.h"

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <boost/smart_ptr.hpp>
#include <pcl/common/common.h>

#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/time_synchronizer.h>

#include <visualization_msgs/msg/marker_array.hpp>

#include <geometry_msgs/msg/pose_stamped.hpp>
#include <tf2_ros/buffer_interface.h>
#include <tf2_ros/visibility_control.h>
#include <tf2/buffer_core.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/transform_stamped.h>
#include <tf2_eigen/tf2_eigen.h>


class WaymoStitcherNode : public rclcpp::Node
{
public:
    using Point = pcl::PointXYZ;
    using Cloud = pcl::PointCloud<pcl::PointXYZ>;

    explicit WaymoStitcherNode();
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr pub_stitched_cloud_;

    message_filters::Subscriber<sensor_msgs::msg::PointCloud2> sub_lid_front_;
    message_filters::Subscriber<sensor_msgs::msg::PointCloud2> sub_lid_right_;
    message_filters::Subscriber<sensor_msgs::msg::PointCloud2> sub_lid_rear_;
    message_filters::Subscriber<sensor_msgs::msg::PointCloud2> sub_lid_left_;
    message_filters::Subscriber<sensor_msgs::msg::PointCloud2> sub_lid_top_;


    std::shared_ptr<message_filters::TimeSynchronizer<
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2,
            sensor_msgs::msg::PointCloud2>> sync_;

    void CallbackSync(const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_front,
                      const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_right,
                      const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_rear,
                      const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_left,
                      const sensor_msgs::msg::PointCloud2::ConstSharedPtr& msg_top);

    std::shared_ptr<tf2_ros::Buffer>            tf_buffer_;
    std::shared_ptr<tf2_ros::TransformListener> tf_listener_;

    rclcpp::Subscription<visualization_msgs::msg::MarkerArray>::SharedPtr sub_lid_labels_;
    std::function<void(std::shared_ptr<visualization_msgs::msg::MarkerArray>)> functionSub_lid_labels_;
    void CallbackSubLidarLabels(const visualization_msgs::msg::MarkerArray::ConstSharedPtr& msg_lidar_labels);

    int id_msg_clouds;
    int id_msg_lid_labels;

    typedef struct TypeLidarLabels
    {
        visualization_msgs::msg::MarkerArray::ConstSharedPtr msg_lidar_labels;
        double sec;
        double nsec;
    }LidarLabels;

    std::deque<LidarLabels> deque_lid_labels;


};

#endif //BUILD_WAYMOSTITCHERNODE_H
