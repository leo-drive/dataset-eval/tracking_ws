//
// Created by goktug on 14.04.2021.
//

#ifndef BUILD_ROSRELATED_H
#define BUILD_ROSRELATED_H

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <Eigen/Dense>
#include <autoware_auto_msgs/msg/detected_objects.hpp>
#include <autoware_auto_msgs/msg/detected_object.hpp>
#include <vision_msgs/msg/bounding_box3_d.hpp>

class ROSRelated {
public:
    using Point = pcl::PointXYZ;
    using Cloud = pcl::PointCloud<pcl::PointXYZ>;

    static void
    PublishPointCloud(const rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr& publisher,
                      const Cloud::Ptr& cloud,
                      const sensor_msgs::msg::PointCloud2::ConstSharedPtr & msg_point_cloud);

};


#endif //BUILD_ROSRELATED_H
