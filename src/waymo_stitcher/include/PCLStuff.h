//
// Created by goktug on 14.04.2021.
//

#ifndef BUILD_PCLSTUFF_H
#define BUILD_PCLSTUFF_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/search/impl/search.hpp>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>

class PCLStuff {
public:
    using Point = pcl::PointXYZ;
    using Cloud = pcl::PointCloud<pcl::PointXYZ>;

    using PointInt = pcl::PointXYZI;
    using CloudInt = pcl::PointCloud<pcl::PointXYZI>;

    static Cloud::Ptr
    DownSample(const Cloud::Ptr& cloud,
               const float& leaf_size);

    static std::pair<std::vector<CloudInt::Ptr>,CloudInt::Ptr>
    EuclideanClustering(const Cloud::Ptr& cloud,
                        const float& tolerance,
                        const int& min_point_count,
                        const int& max_point_count);

    static std::pair<std::vector<CloudInt::Ptr>,CloudInt::Ptr>
    Detect3D(const Cloud::Ptr& cloud);
};


#endif //BUILD_PCLSTUFF_H
