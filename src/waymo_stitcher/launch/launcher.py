from launch import LaunchDescription
import launch_ros.actions

def generate_launch_description():
    return LaunchDescription([
        launch_ros.actions.Node(
            package='waymo_stitcher',
            namespace='tracking_ws',
            executable='waymo_stitcher_node',
            output='screen'),

    ])
