import rclpy
from rclpy.node import Node
from helper import GroundTruthDetectionReader
from helper import PolygonPointGenerator
# Alternative for tf py
from squaternion import Quaternion

from autoware_auto_msgs.msg import DetectedObjects
from autoware_auto_msgs.msg import DetectedObject
from autoware_auto_msgs.msg import ObjectClassification
from nav_msgs.msg import Odometry


# self.get_clock().now().to_msg()
# segment-10335539493577748957_1372_870_1392_870_with_camera_labels.db3

class DetectionPublisherNode(Node):
    def __init__(self):
        super().__init__('detection_publisher_node')

        path_ground_truth = "/home/goktug/projects/tracking_ws/src" \
                            "/waymo_ground_truth_detection_publisher/" \
                            "detection_3d_vehicle_detection_validation.bin"
        segment_name = '10868756386479184868_3000_000_3020_000'
        self.reader = GroundTruthDetectionReader(path_ground_truth, segment_name)
        data_ground_truth = self.reader.giveGroundTruthData()

        self.list_objects_in_single_segment = data_ground_truth[0]
        self.list_ros_secs = data_ground_truth[1]
        self.list_ros_nsecs = data_ground_truth[2]
        self.frame_count = len(self.list_ros_secs)

        self.frame_id_current = 0

        self.polygon_point_generator = PolygonPointGenerator()

        self.pub_detected_objects = self.create_publisher(DetectedObjects, "/detected_objects", 1)
        #self.pub_odometry = self.create_publisher(Odometry, "/detected_objects", 1)

        self.timer = self.create_timer(20, self.CallbackDetectedObjects)

    def CallbackDetectedObjects(self):
        if self.frame_id_current >= self.frame_count-1:
            print("All ground truth detection objects are published.")
            return

        print("Frame id: ", self.frame_id_current)
        print("Sec: ", self.list_ros_secs[self.frame_id_current], "Nsec: ", self.list_ros_nsecs[self.frame_id_current])

        detected_objects = DetectedObjects()
        detected_objects.header.frame_id = "vehicle"
        detected_objects.header.stamp.sec = self.list_ros_secs[self.frame_id_current]
        detected_objects.header.stamp.nanosec = self.list_ros_nsecs[self.frame_id_current]
        # Iterate over ground truth detections:
        for object_gt in self.list_objects_in_single_segment[self.frame_id_current]:

            detected_object = DetectedObject()
            detected_object.kinematics.centroid_position.x = object_gt.object.box.center_x
            detected_object.kinematics.centroid_position.y = object_gt.object.box.center_y
            detected_object.kinematics.centroid_position.z = object_gt.object.box.center_z

            # Set orientation:
            q = Quaternion.from_euler(0, 0, object_gt.object.box.heading, degrees=False)
            quaternion = q.normalize
            detected_object.kinematics.orientation.x = quaternion.x
            detected_object.kinematics.orientation.y = quaternion.y
            detected_object.kinematics.orientation.z = quaternion.z
            detected_object.kinematics.orientation.w = quaternion.w

            # Generate Polygon Points:
            for p in self.polygon_point_generator.generatePolygonPoints(object_gt.object.box.center_x,
                                                                        object_gt.object.box.center_y,
                                                                        object_gt.object.box.width,
                                                                        object_gt.object.box.length):
                detected_object.shape.polygon.points.append(p)
            detected_object.shape.height = object_gt.object.box.height

            # Object Classification:
            object_classification = ObjectClassification()
            object_classification.classification = 1
            object_classification.probability = 1.0
            detected_object.existence_probability = 1.0
            detected_object.classification.append(object_classification)

            # Push detected_object into the detected_objects
            detected_objects.objects.append(detected_object)

        self.pub_detected_objects.publish(detected_objects)

        self.frame_id_current += 1








def main(args=None):
    rclpy.init(args=args)
    node_obj = DetectionPublisherNode()
    rclpy.spin(node_obj)
    node_obj.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
