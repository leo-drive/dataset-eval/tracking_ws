from turtle import Shape

import waymo_open_dataset
from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
from waymo_open_dataset.protos import metrics_pb2
import tensorflow.compat.v1 as tf

from geometry_msgs.msg import Polygon
from geometry_msgs.msg import Point32


class GroundTruthDetectionReader(object):
    def __init__(self, path_ground_truth,
                 segment_name):
        self.path_ground_truth = path_ground_truth
        self.segment_name = segment_name
        self.list_objects_in_single_segment = []
        self.list_time_stamps = []
        self.list_ros_secs = []
        self.list_ros_nsecs = []

        objects = metrics_pb2.Objects()
        f = open(path_ground_truth, 'rb')
        objects.ParseFromString(f.read())
        f.close()

        set_list_time_stamps = []
        # READ GROUND TRUTH DETECTIONS:
        for object in objects.objects:
            if object.context_name == self.segment_name:
                set_list_time_stamps.append(object.frame_timestamp_micros)
        self.list_time_stamps = sorted(list(set(set_list_time_stamps)))

        for time_stamp in self.list_time_stamps:
            frame_stamp_secs = int(time_stamp / 1000000)
            frame_stamp_nsecs = int((time_stamp / 1000000.0 - frame_stamp_secs) * 1000000000)
            self.list_ros_secs.append(frame_stamp_secs)
            self.list_ros_nsecs.append(frame_stamp_nsecs)
            # print("Sec: ", frame_stamp_secs, "Nsec: ", frame_stamp_nsecs)

        for frame_id, time_stamp in enumerate(self.list_time_stamps):
            print("Frame id: ", frame_id)
            print("Sec: ", self.list_ros_secs[frame_id], "Nsec: ", self.list_ros_nsecs[frame_id])
            list_objects_in_single_frame = []

            for object in objects.objects:
                if object.context_name == self.segment_name and time_stamp == object.frame_timestamp_micros:
                    list_objects_in_single_frame.append(object)
                    #print(object)
            self.list_objects_in_single_segment.append(list_objects_in_single_frame)
            print("Object count in a single frame: ", len(list_objects_in_single_frame))
            print("Objects are read.")
            print("################################################################")

    def giveGroundTruthData(self):
        return self.list_objects_in_single_segment, self.list_ros_secs, self.list_ros_nsecs


class PolygonPointGenerator(object):
    def __init__(self):
        pass

    def generatePolygonPoints(self,
                     dx,
                     dy,
                     width,
                     length):
        list_polygon_points = []
        polygon = Polygon()
        # Point: Ön Sol
        p1 = Point32()
        p1.x = dx + length/2
        p1.y = dy + width/2
        p1.z = 0.0
        list_polygon_points.append(p1)
        # Point: Ön Sağ
        p2 = Point32()
        p2.x = dx + length/2
        p2.y = dy - width/2
        p2.z = 0.0
        list_polygon_points.append(p2)
        # Point: Arka Sol
        p5 = Point32()
        p5.x = dx - length/2
        p5.y = dy + width/2
        p5.z = 0.0
        list_polygon_points.append(p5)
        # Point: Arka Sağ
        p7 = Point32()
        p7.x = dx - length/2
        p7.y = dy - width/2
        p7.z = 0.0
        list_polygon_points.append(p7)

        return list_polygon_points


