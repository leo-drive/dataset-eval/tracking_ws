from setuptools import setup

package_name = 'waymo_ground_truth_detection_publisher'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='goktug',
    maintainer_email='yildirimg@mef.edu.tr',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'detection_publisher_node = waymo_ground_truth_detection_publisher.detection_publisher_node:main'
        ],
    },
)
