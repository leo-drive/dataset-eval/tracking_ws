from KalmanFilter import KF
import numpy as np


class TrackedObject(object):
    def __init__(self, initial_pose_x, initial_vel_x, initial_pose_y,
                 initial_vel_y, initial_pose_z, initial_vel_z, object_id, time_stamp_secs,
                 time_stamp_nsecs, bbox3D):
        self.kalman_x = KF(initial_pose_x, initial_vel_x, accel_variance=0.9)
        self.kalman_y = KF(initial_pose_y, initial_vel_y, accel_variance=0.9)
        self.kalman_z = KF(initial_pose_z, initial_vel_z, accel_variance=0.9)

        self.lifetime = 2
        self.meas_variance = 0.5
        self.time_last_seen_secs = time_stamp_secs
        self.time_last_seen_nsecs = time_stamp_nsecs
        self.object_id = object_id

        self.list_meas_pose_x = []
        self.list_meas_pose_y = []
        self.list_meas_pose_z = []
        self.list_meas_pose_x.append(initial_pose_x)
        self.list_meas_pose_y.append(initial_pose_y)
        self.list_meas_pose_z.append(initial_pose_z)

        self.bbox3D = bbox3D

    def predict(self, time_stamp_secs, time_stamp_nsecs):
        dt = time_stamp_secs - self.time_last_seen_secs + (time_stamp_nsecs - self.time_last_seen_nsecs) / 1e9
        (pose_x, vel_x) = self.kalman_x.predict(dt)
        (pose_y, vel_y) = self.kalman_y.predict(dt)
        (pose_z, vel_z) = self.kalman_z.predict(dt)
        self.time_last_seen_secs = time_stamp_secs
        self.time_last_seen_nsecs = time_stamp_nsecs
        # print("Predict() object id: ", self.object_id,pose_x,pose_y,pose_z)

    def update(self, pose_x, pose_y, pose_z, bbox3D):
        self.kalman_x.update(pose_x, self.meas_variance)
        self.kalman_y.update(pose_y, self.meas_variance)
        self.kalman_z.update(pose_z, self.meas_variance)
        self.list_meas_pose_x.append(pose_x)
        self.list_meas_pose_y.append(pose_y)
        self.list_meas_pose_z.append(pose_z)
        self.bbox3D = bbox3D
        self.lifetime = 3

    def getPose(self):
        return self.kalman_x.pos()[0], self.kalman_y.pos()[0], self.kalman_z.pos()[0]

    def getVelocity(self):
        return self.kalman_x.vel(), self.kalman_y.vel(), self.kalman_z.vel()
