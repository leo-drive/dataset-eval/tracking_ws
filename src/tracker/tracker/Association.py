from TrackedObject import TrackedObject
import math
import numpy as np


class Association(object):
    def __init__(self, distance_threshold):
        super().__init__()
        self.distance_threshold = distance_threshold
        self.time_stamp_current_secs = -1
        self.time_stamp_current_nsecs = -1
        self.object_id = 0

    def updateTimeStamp(self, secs, nsecs):
        self.time_stamp_current_secs = secs
        self.time_stamp_current_nsecs = nsecs

    def updateTrackedObjects(self, tracked_objects, detected_objects_current):
        tracked_objects_temp = []
        ids_matched_objects = []
        counter_killed_object = 0
        for tracked_object in tracked_objects:
            distances = []
            positions_x = []
            positions_y = []
            positions_z = []
            for detected_object in detected_objects_current:
                distance = math.sqrt(
                    pow(detected_object.results[0].pose.pose.position.x - tracked_object.getPose()[0], 2) +
                    pow(detected_object.results[0].pose.pose.position.y - tracked_object.getPose()[1], 2) +
                    pow(detected_object.results[0].pose.pose.position.z - tracked_object.getPose()[2], 2))
                distances.append(distance)
                positions_x.append(detected_object.results[0].pose.pose.position.x)
                positions_y.append(detected_object.results[0].pose.pose.position.y)
                positions_z.append(detected_object.results[0].pose.pose.position.z)

            min_id = np.argmin(distances)
            min_distance = distances[min_id]

            if min_distance < self.distance_threshold and min_id in ids_matched_objects:
                print(
                    "Occlusion!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

            # Tracked object matched with detected object (t) : Update
            if min_distance < self.distance_threshold and min_id not in ids_matched_objects:
                tracked_object.update(positions_x[min_id], positions_y[min_id], positions_z[min_id], detected_objects_current[min_id].bbox)
                ids_matched_objects.append(min_id)
                tracked_objects_temp.append(tracked_object)
                print("Tracked object ", tracked_object.object_id, "matched with current detected object ",
                      positions_x[min_id], positions_y[min_id], positions_z[min_id], " distance: ", min_distance)

            # Tracked object not matched with detected object (t) : Reduce lifetime
            else:
                tracked_object.lifetime = tracked_object.lifetime - 1
                print("Tracked object", tracked_object.object_id,
                      "not matched any detected current object lifetime -1.")

                if tracked_object.lifetime < 1:
                    counter_killed_object += 1
                else:
                    tracked_objects_temp.append(tracked_object)

        assert len(tracked_objects) == len(tracked_objects_temp) + counter_killed_object

        detected_objects_unmatched = []
        for id in range(len(detected_objects_current)):
            if id not in ids_matched_objects:
                detected_objects_unmatched.append(detected_objects_current[id])

        assert (len(ids_matched_objects) + len(detected_objects_unmatched) == len(
            detected_objects_current))

        '''print("Tracked object update() matched object count: ", len(ids_matched_objects))
        print("Tracked object update() unmatched object count: ", len(detected_objects_unmatched))
        print("Killed tracked object count: ", counter_killed_object)'''
        print("Tracked object update() unmatched object count: ", len(detected_objects_unmatched))
        print("Killed tracked object count: ", counter_killed_object)

        return tracked_objects_temp, detected_objects_unmatched


    def associateDetectedObjects(self, detected_objects_current_frame,
                                 detected_objects_previous_frame):
        new_tracked_objects = []

        for id1, object_current in enumerate(detected_objects_current_frame):
            distances = []
            for id2, object_previous in enumerate(detected_objects_previous_frame):
                distance = self.giveDistance(object_current, object_previous)
                distances.append(distance)

            min_id = np.argmin(distances)
            min_distance = distances[min_id]

            if min_distance < self.distance_threshold:
                # print("associateDetectedObjects() Current object ",id1, "matched with ", min_id, " distance: ", min_distance)
                new_tracked_object = TrackedObject(object_current.results[0].pose.pose.position.x, 0,
                                                   object_current.results[0].pose.pose.position.y, 0,
                                                   object_current.results[0].pose.pose.position.z, 0,
                                                   self.object_id, self.time_stamp_current_secs,
                                                   self.time_stamp_current_nsecs,
                                                   object_current.bbox)
                new_tracked_objects.append(new_tracked_object)
                self.object_id += 1

            else:

                # Different lane matching:
                if abs(object_current.results[0].pose.pose.position.y) > 3:
                    for id2, object_previous in enumerate(detected_objects_previous_frame):
                        if abs(object_previous.results[0].pose.pose.position.y) > 3:
                            distance = self.giveDistance(object_current, object_previous)
                            if distance < 1.5 * self.distance_threshold:
                                new_tracked_object = TrackedObject(object_current.results[0].pose.pose.position.x, 0,
                                                                   object_current.results[0].pose.pose.position.y, 0,
                                                                   object_current.results[0].pose.pose.position.z, 0,
                                                                   self.object_id, self.time_stamp_current_secs,
                                                                   self.time_stamp_current_nsecs,
                                                                   object_current.bbox)
                                new_tracked_objects.append(new_tracked_object)
                                self.object_id += 1
                                print("**********************************************")
                                print("Different lane matching:")
                                print("Current obj matched: ",object_current.results[0].pose.pose.position.x,
                                      object_current.results[0].pose.pose.position.y,
                                      object_current.results[0].pose.pose.position.z)
                                print("Previous obj matched: ",object_previous.results[0].pose.pose.position.x,
                                      object_previous.results[0].pose.pose.position.y,
                                      object_previous.results[0].pose.pose.position.z)
                                print("**********************************************")

        return new_tracked_objects

    def giveDistance(self, object_current, object_previous):
        result = math.sqrt(pow(
            object_current.results[0].pose.pose.position.x - object_previous.results[0].pose.pose.position.x,
            2) +
                           pow(
                               object_current.results[0].pose.pose.position.y - object_previous.results[0].pose.pose.position.y,
                               2) +
                           pow(
                               object_current.results[0].pose.pose.position.z - object_previous.results[0].pose.pose.position.z,
                               2))
        return result
