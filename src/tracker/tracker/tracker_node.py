import rclpy
from rclpy.node import Node
import autoware_auto_msgs.msg as autoware_auto_msgs
import visualization_msgs.msg as visualization_msgs
from Association import Association

from vision_msgs.msg import Detection3DArray
from vision_msgs.msg import ObjectHypothesisWithPose
from vision_msgs.msg import Detection3D


# self.get_clock().now().to_msg()
# segment-10335539493577748957_1372_870_1392_870_with_camera_labels.db3

class TrackerNode(Node):
    def __init__(self):
        super().__init__('tracker_node')
        self.subscription = self.create_subscription(Detection3DArray, '/detection_3d_array',
                                                     self.CallbackDetectedObjects, 3)

        self.pub_tracked_objects = self.create_publisher(Detection3DArray, "/tracked_objects", 1)
        self.pub_markerArr = self.create_publisher(visualization_msgs.MarkerArray, "/tracking_labels", 0)

        self.Association = Association(3)
        self.tracked_objects = []
        self.detected_objects_current_frame = []
        self.detected_objects_previous_frame = []
        self.message_id = 0

    def CallbackDetectedObjects(self, msg_detection_3D_array):

        self.detected_objects_current_frame = msg_detection_3D_array.detections  # Each instance has the type of vision_msgs/Detection3D.msg
        self.Association.updateTimeStamp(msg_detection_3D_array.header.stamp.sec,
                                         msg_detection_3D_array.header.stamp.nanosec)
        print("Time stamp: ", msg_detection_3D_array.header.stamp.sec, msg_detection_3D_array.header.stamp.nanosec)
        print("Detected object count: ", len(self.detected_objects_current_frame))
        print("Tracked object count: ", len(self.tracked_objects))

        print("Current detected objects:")
        for id, obj in enumerate(self.detected_objects_current_frame):
            print("Id", id, ":", obj.results[0].pose.pose.position.x,
                  obj.results[0].pose.pose.position.y,
                  obj.results[0].pose.pose.position.z)

        # If tracked object exist
        if bool(self.tracked_objects):
            detected_objects_current_tmp = self.detected_objects_current_frame

            # Predict
            for tracked_object in self.tracked_objects:
                tracked_object.predict(msg_detection_3D_array.header.stamp.sec,
                                       msg_detection_3D_array.header.stamp.nanosec)

            # Update by associating tracked objects and detected objects (t)
            self.tracked_objects, detected_objects_unmatched = self.Association.updateTrackedObjects(
                self.tracked_objects, detected_objects_current_tmp)

            print("Tracked objects after update:")
            for id, obj in enumerate(self.tracked_objects):
                print("Id: ", obj.object_id, ":", obj.getPose()[0], obj.getPose()[1], obj.getPose()[2])

            if bool(detected_objects_unmatched):
                # Generate new tracked objects by associating residual detected objects(t) and detected_objects(t-1):
                newly_tracked_objects = self.Association.associateDetectedObjects(detected_objects_unmatched,
                                                                                  self.detected_objects_previous_frame)
                if bool(newly_tracked_objects):
                    for new_tracked_object in newly_tracked_objects:
                        print("New tracked objects is generated with id:", new_tracked_object.object_id)
                        self.tracked_objects.append(new_tracked_object)
                else:
                    print("Unmatched detected object exist, but new tracked object is not generated.")


        # If tracked object doesn't exist
        else:
            if self.message_id > 0:
                # Associating detected objects (t) and detected objects (t-1)
                self.tracked_objects = self.Association.associateDetectedObjects(self.detected_objects_current_frame,
                                                                                 self.detected_objects_previous_frame)

        self.message_id += 1
        self.detected_objects_previous_frame = self.detected_objects_current_frame
        print("********************************************************************************************************")

        # Generate tracked object message with Detection3DArray message
        if bool(self.tracked_objects):
            detection3DArray = Detection3DArray()
            detection3DArray.header.frame_id = "vehicle"
            detection3DArray.header.stamp = msg_detection_3D_array.header.stamp
            for tracked_object in self.tracked_objects:
                detection3D = Detection3D()
                detection3D.header.frame_id = "vehicle"
                detection3D.header.stamp = msg_detection_3D_array.header.stamp
                detection3D.bbox = tracked_object.bbox3D
                detection3D.tracking_id = str(tracked_object.object_id)
                #detection3D.source_cloud =

                objectHypothesisWithPose = ObjectHypothesisWithPose()
                objectHypothesisWithPose.pose.pose.position.x = tracked_object.getPose()[0]
                objectHypothesisWithPose.pose.pose.position.y = tracked_object.getPose()[1]
                objectHypothesisWithPose.pose.pose.position.z = tracked_object.getPose()[2]
                objectHypothesisWithPose.pose.pose.orientation.x = 1.0
                objectHypothesisWithPose.pose.pose.orientation.y = 0.0
                objectHypothesisWithPose.pose.pose.orientation.z = 0.0
                objectHypothesisWithPose.pose.pose.orientation.w = 1.0
                objectHypothesisWithPose.id = str(tracked_object.object_id)
                objectHypothesisWithPose.score = 1.0
                detection3D.is_tracking = True

                detection3D.results.append(objectHypothesisWithPose)
                detection3DArray.detections.append(detection3D)

            self.pub_tracked_objects.publish(detection3DArray)

            markerArray = visualization_msgs.MarkerArray()
            for id_ , tracked_object in enumerate(self.tracked_objects):
                marker = visualization_msgs.Marker()
                marker.action = visualization_msgs.Marker.ADD
                marker.header.frame_id = "vehicle"
                marker.header.stamp = msg_detection_3D_array.header.stamp
                marker.id = id_
                marker.type = visualization_msgs.Marker.TEXT_VIEW_FACING
                marker.pose.position.x = tracked_object.getPose()[0]
                marker.pose.position.y = tracked_object.getPose()[1]
                marker.pose.position.z = tracked_object.getPose()[2]+1
                marker.scale.x = 10.0
                marker.scale.y = 10.0
                marker.scale.z = 10.0
                marker.color.r = 1.0
                marker.color.g = 1.0
                marker.color.b = 1.0
                marker.color.a = 1.0
                marker.pose.orientation.x = 0.0
                marker.pose.orientation.y = 0.0
                marker.pose.orientation.z = 0.0
                marker.pose.orientation.w = 1.0
                marker.text = str(tracked_object.object_id)
                markerArray.markers.append(marker)
            self.pub_markerArr.publish(markerArray)






        '''for id, obj in enumerate(self.detected_objects_current_frame):
            print(obj.results[0].id)
            print("******")
            print(obj.results[0].pose.pose.position.x)
            print("******")
            print(obj.bbox.center.position.x)'''

        '''self.detected_objects_current_frame = msg.objects
        self.Association.updateTimeStamp(msg.header.stamp.sec, msg.header.stamp.nanosec)

        print("Time stamp: ", msg.header.stamp.sec, msg.header.stamp.nanosec)
        print("Detected object count: ", len(self.detected_objects_current_frame))
        print("Tracked object count: ", len(self.tracked_objects))

        print("Current detected objects:")
        for id, obj in enumerate(self.detected_objects_current_frame):
            print("Id", id, ":", obj.kinematics.pose.pose.position.x, obj.kinematics.pose.pose.position.y,
                  obj.kinematics.pose.pose.position.z)

        # If tracked object exist
        if bool(self.tracked_objects):
            detected_objects_current_tmp = self.detected_objects_current_frame

            # Predict
            for tracked_object in self.tracked_objects:
                tracked_object.predict(msg.header.stamp.sec, msg.header.stamp.nanosec)

            # Update by associating tracked objects and detected objects (t)
            self.tracked_objects, detected_objects_unmatched = self.Association.updateTrackedObjects(
                self.tracked_objects, detected_objects_current_tmp)

            print("Tracked objects after update:")
            for id, obj in enumerate(self.tracked_objects):
                print("Id: ", obj.object_id, ":", obj.getPose()[0], obj.getPose()[1], obj.getPose()[2])

            if bool(detected_objects_unmatched):
                # Generate new tracked objects by associating residual detected objects(t) and detected_objects(t-1):
                newly_tracked_objects = self.Association.associateDetectedObjects(detected_objects_unmatched,
                                                                                  self.detected_objects_previous_frame)
                if bool(newly_tracked_objects):
                    for new_tracked_object in newly_tracked_objects:
                        print("New tracked objects is generated with id:", new_tracked_object.object_id)
                        self.tracked_objects.append(new_tracked_object)
                else:
                    print("Unmatched detected object exist, but new tracked object is not generated.")


        # If tracked object doesn't exist
        else:
            if self.message_id > 0:
                # Associating detected objects (t) and detected objects (t-1)
                self.tracked_objects = self.Association.associateDetectedObjects(self.detected_objects_current_frame,
                                                                                 self.detected_objects_previous_frame)

        self.message_id += 1
        self.detected_objects_previous_frame = self.detected_objects_current_frame


        print("********************************************************************************************************")

        if bool(self.tracked_objects):
            autoware_tracked_objects = autoware_auto_msgs.TrackedObjects()
            autoware_tracked_objects.header.frame_id = "vehicle"
            autoware_tracked_objects.header.stamp = msg.header.stamp
            for tracked_object in self.tracked_objects:
                autoware_tracked_object = autoware_auto_msgs.TrackedObject()
                autoware_tracked_object.kinematics.pose.pose.position.x = tracked_object.getPose()[0]
                autoware_tracked_object.kinematics.pose.pose.position.y = tracked_object.getPose()[1]
                autoware_tracked_object.kinematics.pose.pose.position.z = tracked_object.getPose()[2]
                autoware_tracked_object.object_id = tracked_object.object_id
                autoware_tracked_objects.objects.append(autoware_tracked_object)
            self.pub_tracked_objects.publish(autoware_tracked_objects)

        markerArray_bbox_id_arrow = visualization_msgs.MarkerArray()
        for id_ , tracked_object in enumerate(self.tracked_objects):
            marker = visualization_msgs.Marker()
            marker.action = visualization_msgs.Marker.ADD
            marker.header.frame_id = "vehicle"
            marker.header.stamp = msg.header.stamp
            marker.id = id_
            marker.type = visualization_msgs.Marker.TEXT_VIEW_FACING
            marker.pose.position.x = tracked_object.getPose()[0]
            marker.pose.position.y = tracked_object.getPose()[1]
            marker.pose.position.z = tracked_object.getPose()[2]+1
            marker.scale.x = 10.0
            marker.scale.y = 10.0
            marker.scale.z = 10.0
            marker.color.r = 1.0
            marker.color.g = 1.0
            marker.color.b = 1.0
            marker.color.a = 1.0
            marker.pose.orientation.x = 0.0
            marker.pose.orientation.y = 0.0
            marker.pose.orientation.z = 0.0
            marker.pose.orientation.w = 1.0
            marker.text = str(tracked_object.object_id)
            markerArray_bbox_id_arrow.markers.append(marker)
        self.pub_markerArr.publish(markerArray_bbox_id_arrow)'''


def main(args=None):
    rclpy.init(args=args)
    node_obj = TrackerNode()
    rclpy.spin(node_obj)
    node_obj.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
