### Launch
```bash
ros2 launch tracking_visualizer launcher.py
```
###Params
```bash
sub_topic_tracked_objects: "/tracked_objects"
pub_topic_bbox_id_arrow: "/markerArray_tracked_objects"
pub_topic_polygon_points: "/markerArray_polygon_points"
pub_topic_circle: "/markerArray_tracked_objects_circle"
radius_circle: 1.5
```

