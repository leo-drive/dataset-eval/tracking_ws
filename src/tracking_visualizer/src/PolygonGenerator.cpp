//
// Created by goktug on 20.05.2021.
//

#include "PolygonGenerator.h"




PolygonGenerator::PolygonGenerator() {
}

geometry_msgs::msg::Polygon
PolygonGenerator::GeneratePolygon1(const autoware_auto_msgs::msg::TrackedObject &trackedObject) {

    // Artificial polygon points generator:
    float length = 2.5; // x
    float width = 1.8; // y
    float height = 1.5; // z

    geometry_msgs::msg::Polygon polygon;

    geometry_msgs::msg::Point32 p1; // ön sol üst
    p1.x = trackedObject.kinematics.centroid_position.x + length/2;
    p1.y = trackedObject.kinematics.centroid_position.y + width/2;
    p1.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p1);

    geometry_msgs::msg::Point32 p2; // ön sağ üst
    p2.x = trackedObject.kinematics.centroid_position.x + length/2;
    p2.y = trackedObject.kinematics.centroid_position.y - width/2;
    p2.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p2);

    geometry_msgs::msg::Point32 p3; // ön sol alt
    p3.x = trackedObject.kinematics.centroid_position.x + length/2;
    p3.y = trackedObject.kinematics.centroid_position.y + width/2;
    p3.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p3);

    geometry_msgs::msg::Point32 p4; // ön sağ alt
    p4.x = trackedObject.kinematics.centroid_position.x + length/2;
    p4.y = trackedObject.kinematics.centroid_position.y - width/2;
    p4.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p4);

    geometry_msgs::msg::Point32 p5; // arka sol üst
    p5.x = trackedObject.kinematics.centroid_position.x - length/2;
    p5.y = trackedObject.kinematics.centroid_position.y + width/2;
    p5.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p5);

    geometry_msgs::msg::Point32 p6; // arka sol alt
    p6.x = trackedObject.kinematics.centroid_position.x - length/2;
    p6.y = trackedObject.kinematics.centroid_position.y + width/2;
    p6.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p6);

    geometry_msgs::msg::Point32 p7; // arka sağ üst
    p7.x = trackedObject.kinematics.centroid_position.x - length/2;
    p7.y = trackedObject.kinematics.centroid_position.y - width/2;
    p7.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p7);

    geometry_msgs::msg::Point32 p8; // arka sağ alt
    p8.x = trackedObject.kinematics.centroid_position.x - length/2;
    p8.y = trackedObject.kinematics.centroid_position.y - width/2;
    p8.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p8);

    return polygon;
}


geometry_msgs::msg::Polygon
PolygonGenerator::GeneratePolygon2(const autoware_auto_msgs::msg::TrackedObject &trackedObject) {

    // Artificial polygon points generator:
    float length = 2; // x
    float width = 1.2; // y
    float height = 1; // z

    geometry_msgs::msg::Polygon polygon;

    geometry_msgs::msg::Point32 p1; // ön sol üst
    p1.x = trackedObject.kinematics.centroid_position.x + length/2;
    p1.y = trackedObject.kinematics.centroid_position.y + width/2;
    p1.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p1);

    geometry_msgs::msg::Point32 p2; // ön sağ üst
    p2.x = trackedObject.kinematics.centroid_position.x + length/2;
    p2.y = trackedObject.kinematics.centroid_position.y - width/2;
    p2.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p2);

    geometry_msgs::msg::Point32 p3; // ön sol alt
    p3.x = trackedObject.kinematics.centroid_position.x + length/2;
    p3.y = trackedObject.kinematics.centroid_position.y + width/2;
    p3.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p3);

    geometry_msgs::msg::Point32 p4; // ön sağ alt
    p4.x = trackedObject.kinematics.centroid_position.x + length/2;
    p4.y = trackedObject.kinematics.centroid_position.y - width/2;
    p4.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p4);

    geometry_msgs::msg::Point32 p5; // arka sol üst
    p5.x = trackedObject.kinematics.centroid_position.x - length/2;
    p5.y = trackedObject.kinematics.centroid_position.y + width/2;
    p5.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p5);

    geometry_msgs::msg::Point32 p6; // arka sol alt
    p6.x = trackedObject.kinematics.centroid_position.x - length/2;
    p6.y = trackedObject.kinematics.centroid_position.y + width/2;
    p6.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p6);

    geometry_msgs::msg::Point32 p7; // arka sağ üst
    p7.x = trackedObject.kinematics.centroid_position.x - length/2;
    p7.y = trackedObject.kinematics.centroid_position.y - width/2;
    p7.z = trackedObject.kinematics.centroid_position.z + height/2;
    polygon.points.push_back(p7);

    geometry_msgs::msg::Point32 p8; // arka sağ alt
    p8.x = trackedObject.kinematics.centroid_position.x - length/2;
    p8.y = trackedObject.kinematics.centroid_position.y - width/2;
    p8.z = trackedObject.kinematics.centroid_position.z - height/2;
    polygon.points.push_back(p8);

    return polygon;
}
