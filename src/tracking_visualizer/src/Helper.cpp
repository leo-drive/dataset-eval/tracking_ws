//
// Created by goktug on 18.05.2021.
//

#include "Helper.h"

Helper::Helper()
{
    std::cout << "Helper is initialized." << std::endl;
}


void
Helper::generateBBox(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                     visualization_msgs::msg::MarkerArray& markerArray) {

    // Iterate over tracked objects:
    for(const autoware_auto_msgs::msg::TrackedObject& trackedObject:trackedObjects.objects){
        // Iterate over shapes:
        std::vector<geometry_msgs::msg::Point32> vector_polygon_points;
        for(const autoware_auto_msgs::msg::Shape& shape:trackedObject.shape)
        {
            // Iterate over polygon points:
            for(const geometry_msgs::msg::Point32& point_polygon:shape.polygon.points){
                vector_polygon_points.push_back(point_polygon);
            }
        }
        std::vector<float> vector_x_values;
        std::vector<float> vector_y_values;
        std::vector<float> vector_z_values;
        for(const auto& point:vector_polygon_points){
            vector_x_values.push_back(point.x);
            vector_y_values.push_back(point.y);
            vector_z_values.push_back(point.z);
        }
        auto max_x = std::max_element(vector_x_values.begin(),vector_x_values.end());
        auto min_x = std::min_element(vector_x_values.begin(),vector_x_values.end());
        auto max_y = std::max_element(vector_y_values.begin(),vector_y_values.end());
        auto min_y = std::min_element(vector_y_values.begin(),vector_y_values.end());
        auto max_z = std::max_element(vector_z_values.begin(),vector_z_values.end());
        auto min_z = std::min_element(vector_z_values.begin(),vector_z_values.end());
        float length_3DBBox = *max_x-*min_x;
        float width_3DBBox = *max_y-*min_y;
        float height_3DBBox = *max_z-*min_z;

        visualization_msgs::msg::Marker marker_BBox3D;
        marker_BBox3D.ns = "bbox";
        marker_BBox3D.id = trackedObject.object_id;
        marker_BBox3D.header = trackedObjects.header;
        marker_BBox3D.type = visualization_msgs::msg::Marker::CUBE;
        marker_BBox3D.action = visualization_msgs::msg::Marker::ADD;
        marker_BBox3D.pose.position.x = trackedObject.kinematics.centroid_position.x;
        marker_BBox3D.pose.position.y = trackedObject.kinematics.centroid_position.y;
        marker_BBox3D.pose.position.z = trackedObject.kinematics.centroid_position.z;
        marker_BBox3D.pose.orientation.x = trackedObject.kinematics.orientation.x;
        marker_BBox3D.pose.orientation.y = trackedObject.kinematics.orientation.y;
        marker_BBox3D.pose.orientation.z = trackedObject.kinematics.orientation.z;
        marker_BBox3D.pose.orientation.w = trackedObject.kinematics.orientation.w;
        marker_BBox3D.scale.x = length_3DBBox;
        marker_BBox3D.scale.y = width_3DBBox;
        marker_BBox3D.scale.z = height_3DBBox;
        marker_BBox3D.color.a = 0.5;
        marker_BBox3D.color.b = 0;
        marker_BBox3D.color.g = 0;
        marker_BBox3D.color.r = 255;
        builtin_interfaces::msg::Duration duration_filetime;
        duration_filetime.sec = 100;
        duration_filetime.nanosec = 0;
        marker_BBox3D.lifetime = duration_filetime;

        markerArray.markers.push_back(marker_BBox3D);
    }

    // Delete redundant markers:
    if(!vector_ids_must_be_deleted.empty()){
        for(const int& id_will_be_deleted:vector_ids_must_be_deleted){
            visualization_msgs::msg::Marker marker_BBox3D;
            marker_BBox3D.ns = "bbox";
            marker_BBox3D.id = id_will_be_deleted;
            marker_BBox3D.header = trackedObjects.header;
            marker_BBox3D.type = visualization_msgs::msg::Marker::CUBE;
            marker_BBox3D.action = visualization_msgs::msg::Marker::DELETE;
            markerArray.markers.push_back(marker_BBox3D);
        }
    }
}

void
Helper::generateTextObjectId(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                             visualization_msgs::msg::MarkerArray& markerArray) {
    // Iterate over tracked objects:
    for(const autoware_auto_msgs::msg::TrackedObject& trackedObject:trackedObjects.objects) {
        visualization_msgs::msg::Marker marker_TextID;
        marker_TextID.ns = "id";
        marker_TextID.id = trackedObject.object_id;
        marker_TextID.header = trackedObjects.header;
        marker_TextID.type = visualization_msgs::msg::Marker::TEXT_VIEW_FACING;
        marker_TextID.action = visualization_msgs::msg::Marker::ADD;
        marker_TextID.pose.position.x = trackedObject.kinematics.centroid_position.x;
        marker_TextID.pose.position.y = trackedObject.kinematics.centroid_position.y;
        marker_TextID.pose.position.z = trackedObject.kinematics.centroid_position.z + 1;
        marker_TextID.pose.orientation.x = 0;
        marker_TextID.pose.orientation.y = 0;
        marker_TextID.pose.orientation.z = 0;
        marker_TextID.pose.orientation.w = 1;
        marker_TextID.scale.x = 1.0;
        marker_TextID.scale.y = 1.0;
        marker_TextID.scale.z = 1.0;
        marker_TextID.color.r = 1.0;
        marker_TextID.color.g = 1.0;
        marker_TextID.color.b = 1.0;
        marker_TextID.color.a = 1.0;
        marker_TextID.text = std::to_string(trackedObject.object_id);
        builtin_interfaces::msg::Duration duration_filetime;
        duration_filetime.sec = 100;
        duration_filetime.nanosec = 0;
        marker_TextID.lifetime = duration_filetime;
        markerArray.markers.push_back(marker_TextID);
    }

    // Delete redundant markers:
    if(!vector_ids_must_be_deleted.empty()){
        for(const int& id_will_be_deleted:vector_ids_must_be_deleted){
            visualization_msgs::msg::Marker marker_TextID;
            marker_TextID.ns = "id";
            marker_TextID.id = id_will_be_deleted;
            marker_TextID.header = trackedObjects.header;
            marker_TextID.type = visualization_msgs::msg::Marker::TEXT_VIEW_FACING;
            marker_TextID.action = visualization_msgs::msg::Marker::DELETE;
            markerArray.markers.push_back(marker_TextID);
        }
    }
}

void
Helper::generateArrowHeading(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                             visualization_msgs::msg::MarkerArray& markerArray) {
    // Iterate over tracked objects:
    for(const autoware_auto_msgs::msg::TrackedObject& trackedObject:trackedObjects.objects) {
        visualization_msgs::msg::Marker marker_ARROW;
        marker_ARROW.ns = "arrow";
        marker_ARROW.id = trackedObject.object_id;
        marker_ARROW.header = trackedObjects.header;
        marker_ARROW.type = visualization_msgs::msg::Marker::ARROW;
        marker_ARROW.action = visualization_msgs::msg::Marker::ADD;

       tf2::Quaternion q(
                trackedObject.kinematics.orientation.x,
                trackedObject.kinematics.orientation.y,
                trackedObject.kinematics.orientation.z,
                trackedObject.kinematics.orientation.w);
        tf2::Matrix3x3 m(q);
        double roll, pitch, yaw;
        m.getRPY(roll, pitch, yaw);

        float start_x = trackedObject.kinematics.centroid_position.x;
        float start_y = trackedObject.kinematics.centroid_position.y;
        float start_z = trackedObject.kinematics.centroid_position.z;
        float ratio = 2;
        float end_x = start_x + cos(yaw)*ratio;
        float end_y = start_y + sin(yaw)*ratio;
        float end_z = start_z;

        geometry_msgs::msg::Point p1,p2;
        p1.x = start_x;
        p1.y = start_y;
        p1.z = start_z;
        p2.x = end_x;
        p2.y = end_y;
        p2.z = end_z;
        marker_ARROW.points.push_back(p1);
        marker_ARROW.points.push_back(p2);

        builtin_interfaces::msg::Duration duration_filetime;
        duration_filetime.sec = 100;
        duration_filetime.nanosec = 0;
        marker_ARROW.lifetime = duration_filetime;

        marker_ARROW.color.a = 1.0;
        marker_ARROW.color.r = 0.5;
        marker_ARROW.color.g = 1.0;
        marker_ARROW.color.b = 0.5;

        marker_ARROW.scale.x = 0.1;
        marker_ARROW.scale.y = 0.1;
        marker_ARROW.scale.z = 0.4;

        markerArray.markers.push_back(marker_ARROW);
    }
    // Delete redundant markers:
    if(!vector_ids_must_be_deleted.empty()){
        for(const int& id_will_be_deleted:vector_ids_must_be_deleted){
            visualization_msgs::msg::Marker marker_ARROW;
            marker_ARROW.ns = "arrow";
            marker_ARROW.id = id_will_be_deleted;
            marker_ARROW.header = trackedObjects.header;
            marker_ARROW.type = visualization_msgs::msg::Marker::ARROW;
            marker_ARROW.action = visualization_msgs::msg::Marker::DELETE;
            markerArray.markers.push_back(marker_ARROW);
        }
    }
}


void
Helper::generateCircleWithRadius(const autoware_auto_msgs::msg::TrackedObjects &trackedObjects,
                                 visualization_msgs::msg::MarkerArray &markerArray,
                                 const float &radius) {

    auto d2r = [](const float& d){
        return (d / 180.0) * ((double) M_PI);
    };

    // Iterate over tracked objects:
    for(const autoware_auto_msgs::msg::TrackedObject& trackedObject:trackedObjects.objects) {
        visualization_msgs::msg::Marker marker_circle;
        marker_circle.header = trackedObjects.header;
        marker_circle.ns = "circle";
        marker_circle.id = trackedObject.object_id;
        marker_circle.type = visualization_msgs::msg::Marker::LINE_STRIP;
        marker_circle.action = visualization_msgs::msg::Marker::ADD;

        // POINTS markers use x and y scale for width/height respectively
        marker_circle.scale.x = 0.02;
        marker_circle.color.g = 1.0f;
        marker_circle.color.a = 1.0;

        float degree = 360;
        while(degree > 0.1) {

            float x_outer = radius * cos(d2r(degree));
            float y_outer = radius * sin(d2r(degree));
            geometry_msgs::msg::Point p_outer;
            p_outer.x = trackedObject.kinematics.centroid_position.x + x_outer;
            p_outer.y = trackedObject.kinematics.centroid_position.y + y_outer;
            p_outer.z = trackedObject.kinematics.centroid_position.z - 0.2;
            marker_circle.points.push_back(p_outer);
            degree -= 0.2;
        }
        markerArray.markers.push_back(marker_circle);
    }
    // Delete redundant markers:
    if(!vector_ids_must_be_deleted.empty()){
        for(const int& id_will_be_deleted:vector_ids_must_be_deleted){
            visualization_msgs::msg::Marker marker_circle;
            marker_circle.ns = "circle";
            marker_circle.id = id_will_be_deleted;
            marker_circle.header = trackedObjects.header;
            marker_circle.type = visualization_msgs::msg::Marker::LINE_STRIP;
            marker_circle.action = visualization_msgs::msg::Marker::DELETE;
            markerArray.markers.push_back(marker_circle);
        }
    }
}

void
Helper::visualizePolygonPoints(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                               pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_polygon_point,
                               const rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr& publisher) {
    // Iterate over tracked objects:
    for(const autoware_auto_msgs::msg::TrackedObject& tracked_object:trackedObjects.objects){
        // Iterate over shapes:
        for(const autoware_auto_msgs::msg::Shape& shape:tracked_object.shape)
        {
            // Iterate over polygon points:
            for(const geometry_msgs::msg::Point32& point_polygon:shape.polygon.points){
                pcl::PointXYZ point_pcl;
                point_pcl.x = point_polygon.x;
                point_pcl.y = point_polygon.y;
                point_pcl.z = point_polygon.z;
                cloud_polygon_point->points.push_back(point_pcl);
            }
        }
    }
    sensor_msgs::msg::PointCloud2 msg_cloud;
    pcl::toROSMsg(*cloud_polygon_point, msg_cloud);
    msg_cloud.header = trackedObjects.header;
    publisher->publish(msg_cloud);
}

void
Helper::setObjectIds(const autoware_auto_msgs::msg::TrackedObjects &trackedObjects) {
    std::vector<int> vector_ids_tmp;
    for(const autoware_auto_msgs::msg::TrackedObject& trackedObject:trackedObjects.objects)
        vector_ids_tmp.push_back(trackedObject.object_id);
    vector_ids_current_frame = vector_ids_tmp;

    /*
    std::cout << "Tracked object ids current frame:" << std::endl;
    for(const auto &id : vector_ids_current_frame)
        std::cout << id << std::endl;
    std::cout << "Tracked object ids previous frame:" << std::endl;
    for(const auto &id : vector_ids_previous_frame)
        std::cout << id << std::endl;
    */
}

void
Helper::determineObjectIdsWillBeDeleted() {
    std::vector<int> vector_ids_tmp;
    if(!vector_ids_previous_frame.empty()){
        for(const int& id_prev_frame:vector_ids_previous_frame){
            bool bool_delete = true;
            for(const int& id_curr_frame:vector_ids_current_frame){
                if(id_prev_frame == id_curr_frame){
                    bool_delete = false;
                    //std::cout << "bool delete:" << bool_delete << std::endl;
                    break;
                }
            }
            if(bool_delete)
                vector_ids_tmp.push_back(id_prev_frame);
        }
        vector_ids_must_be_deleted = vector_ids_tmp;
    }
    /*std::cout << "Tracked object ids must be deleted:" << std::endl;
    for(const auto& id: vector_ids_must_be_deleted)
        std::cout << id << std::endl;*/
}

void
Helper::updateState() {
    vector_ids_previous_frame = vector_ids_current_frame;
}

bool Helper::objectMustBeDeleted(const int& id) {
    bool bool_must_be_deleted = true;
    if(!vector_ids_must_be_deleted.empty()){

        for(const int& id_in_deleted_list:vector_ids_must_be_deleted){
            if(id == id_in_deleted_list){
                bool_must_be_deleted = false;
                break;
            }
        }
    }else{
        bool_must_be_deleted = false;
    }
    return bool_must_be_deleted;
}

void Helper::generatePolygonPoints(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                                   visualization_msgs::msg::MarkerArray& markerArray) {

    std::vector<std::vector<geometry_msgs::msg::Point32>> vector_polygon_points_all;
    // Iterate over tracked objects:
    for(const autoware_auto_msgs::msg::TrackedObject& tracked_object:trackedObjects.objects){
        std::vector<geometry_msgs::msg::Point32> vector_polygon_points_single_object;
        // Iterate over shapes:
        for(const autoware_auto_msgs::msg::Shape& shape:tracked_object.shape)
        {
            // Iterate over polygon points:
            for(const geometry_msgs::msg::Point32& point_polygon:shape.polygon.points)
                vector_polygon_points_single_object.push_back(point_polygon);
        }
        vector_polygon_points_all.push_back(vector_polygon_points_single_object);
    }

    // Iterate over tracked objects:
    for(int i=0; i<trackedObjects.objects.size(); i++) {
        visualization_msgs::msg::Marker marker_Polygon_Points;
        marker_Polygon_Points.ns = "polygon_points";
        marker_Polygon_Points.id = trackedObjects.objects[i].object_id;
        marker_Polygon_Points.header = trackedObjects.header;
        marker_Polygon_Points.type = visualization_msgs::msg::Marker::SPHERE_LIST;
        marker_Polygon_Points.action = visualization_msgs::msg::Marker::ADD;

        for(const auto polygon_point:vector_polygon_points_all[i])
        {
            geometry_msgs::msg::Point p;
            p.x = polygon_point.x;
            p.y = polygon_point.y;
            p.z = polygon_point.z;
            marker_Polygon_Points.points.push_back(p);
        }

        builtin_interfaces::msg::Duration duration_filetime;
        duration_filetime.sec = 100;
        duration_filetime.nanosec = 0;
        marker_Polygon_Points.lifetime = duration_filetime;

        marker_Polygon_Points.color.a = 1.0;
        marker_Polygon_Points.color.r = 0.5;
        marker_Polygon_Points.color.g = 1.0;
        marker_Polygon_Points.color.b = 0.5;

        marker_Polygon_Points.scale.x = 0.1;
        marker_Polygon_Points.scale.y = 0.1;
        marker_Polygon_Points.scale.z = 0.1;

        markerArray.markers.push_back(marker_Polygon_Points);
    }

    // Delete redundant markers:
    if(!vector_ids_must_be_deleted.empty()){
        for(const int& id_will_be_deleted:vector_ids_must_be_deleted){
            visualization_msgs::msg::Marker marker_Polygon_Points;
            marker_Polygon_Points.ns = "polygon_points";
            marker_Polygon_Points.id = id_will_be_deleted;
            marker_Polygon_Points.header = trackedObjects.header;
            marker_Polygon_Points.type = visualization_msgs::msg::Marker::SPHERE_LIST;
            marker_Polygon_Points.action = visualization_msgs::msg::Marker::DELETE;
            markerArray.markers.push_back(marker_Polygon_Points);
        }
    }


}



