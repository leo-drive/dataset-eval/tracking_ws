#include "TrackingVisualizerNode.h"

TrackingVisualizerNode::TrackingVisualizerNode()
:Node("tracking_visualizer_node"),
id_tracked_obj_msg(0)
{
    // Get params:
    std::string path_yaml_params = std::filesystem::current_path();
    path_yaml_params = path_yaml_params + "/src/tracking_visualizer/params/params.yaml";
    YAML::Node map = YAML::LoadFile(path_yaml_params);
    for(YAML::const_iterator it=map.begin(); it!=map.end(); ++it){
        const std::string &key= it->first.as<std::string>();
        const std::string &value = it->second.as<std::string>();;
        params[key] = value;
    }

    functionSub_tracked_objects_ = std::bind(&TrackingVisualizerNode::CallbackTrackedObjects, this, std::placeholders::_1);
    sub_tracked_objects_ = this->create_subscription<autoware_auto_msgs::msg::TrackedObjects>(
            params["sub_topic_tracked_objects"], 10, functionSub_tracked_objects_);
    //pub_cloud_polygon_points_ = create_publisher<sensor_msgs::msg::PointCloud2>(params["pub_topic_polygon_points"], 10);
    pub_markerArray_polygon_points_ = create_publisher<visualization_msgs::msg::MarkerArray>(params["pub_topic_polygon_points"], 10);
    pub_markerArray_bbox_id_arrow_ = create_publisher<visualization_msgs::msg::MarkerArray>(params["pub_topic_bbox_id_arrow"], 10);
    pub_markerArray_cirlce_ = create_publisher<visualization_msgs::msg::MarkerArray>(params["pub_topic_circle"], 10);


    // For test:

    timer_tracked_obj_generator_ = this->create_wall_timer(
            std::chrono::milliseconds(1000),
            [this] { CallbackTrackedObjectGenerator(); });
    pub_tracked_objects = create_publisher<autoware_auto_msgs::msg::TrackedObjects>("/tracked_objects", 10);

}

void TrackingVisualizerNode::CallbackTrackedObjects(
        const autoware_auto_msgs::msg::TrackedObjects::ConstSharedPtr
        &msg_tracked_objects) {

    autoware_auto_msgs::msg::TrackedObjects trackedObjects = *msg_tracked_objects;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_polygon_points(new pcl::PointCloud<pcl::PointXYZ>);

    helper_ptr->setObjectIds(trackedObjects);
    helper_ptr->determineObjectIdsWillBeDeleted();
    helper_ptr->generatePolygonPoints(trackedObjects, markerArray_polygon_points);
    helper_ptr->generateBBox(trackedObjects, markerArray_bbox_id_arrow);
    helper_ptr->generateTextObjectId(trackedObjects, markerArray_bbox_id_arrow);
    helper_ptr->generateArrowHeading(trackedObjects, markerArray_bbox_id_arrow);
    helper_ptr->generateCircleWithRadius(trackedObjects, markerArray_cirlce, std::stof(params["radius_circle"]));
    helper_ptr->updateState();

    pub_markerArray_bbox_id_arrow_->publish(markerArray_bbox_id_arrow);
    pub_markerArray_cirlce_->publish(markerArray_cirlce);
    pub_markerArray_polygon_points_->publish(markerArray_polygon_points);

    markerArray_bbox_id_arrow.markers.clear();
    markerArray_cirlce.markers.clear();
    markerArray_polygon_points.markers.clear();

    RCLCPP_INFO(this->get_logger(), "Tracked objects are visualized.");
}


void
TrackingVisualizerNode::CallbackTrackedObjectGenerator()
{
    autoware_auto_msgs::msg::TrackedObjects trackedObjects;
    autoware_auto_msgs::msg::TrackedObject trackedObject;
    autoware_auto_msgs::msg::TrackedObject trackedObject2;

    trackedObject2.kinematics.centroid_position.x = float(id_tracked_obj_msg)*1;
    trackedObject2.kinematics.centroid_position.y = 5;
    trackedObject2.kinematics.centroid_position.z = 0;
    trackedObject2.kinematics.orientation.x = 0;
    trackedObject2.kinematics.orientation.y = 0;
    trackedObject2.kinematics.orientation.z = 0;
    trackedObject2.kinematics.orientation.w = 1;
    trackedObject2.existence_probability = 0.8;
    trackedObject2.object_id = 0000; // unsigned long

    // Object classification:
    autoware_auto_msgs::msg::ObjectClassification objectClassification;
    objectClassification.classification = 1;
    objectClassification.probability = 0.9;
    trackedObject2.existence_probability = 1;
    trackedObject2.classification.push_back(objectClassification);
    trackedObject.classification.push_back(objectClassification);

    autoware_auto_msgs::msg::Shape shape;
    auto polygon = PolygonGenerator::GeneratePolygon1(trackedObject2);
    shape.polygon = polygon;
    trackedObject2.shape.push_back(shape);

    if(id_tracked_obj_msg < 8){
        trackedObject.kinematics.centroid_position.x = float(id_tracked_obj_msg)*1;
        trackedObject.kinematics.centroid_position.y = 0;
        trackedObject.kinematics.centroid_position.z = 0;
        trackedObject.kinematics.orientation.x = 0;
        trackedObject.kinematics.orientation.y = 0;
        trackedObject.kinematics.orientation.z = 0;
        trackedObject.kinematics.orientation.w= 1;
        trackedObject.existence_probability = 0.8;
        trackedObject.object_id = 0001; // unsigned long

        autoware_auto_msgs::msg::Shape shape;
        auto polygon = PolygonGenerator::GeneratePolygon1(trackedObject);
        shape.polygon = polygon;
        trackedObject.shape.push_back(shape);

        trackedObjects.header.frame_id = "map";
        trackedObjects.header.stamp = this->get_clock()->now();

    }else{
        trackedObject.kinematics.centroid_position.x = float(id_tracked_obj_msg)*1;
        trackedObject.kinematics.centroid_position.y = 0;
        trackedObject.kinematics.centroid_position.z = 0;
        trackedObject.kinematics.orientation.x = 0;
        trackedObject.kinematics.orientation.y = 0;
        trackedObject.kinematics.orientation.z = 0;
        trackedObject.kinematics.orientation.w = 1;
        trackedObject.existence_probability = 0.8;
        trackedObject.object_id = 0002; // unsigned long

        autoware_auto_msgs::msg::Shape shape;
        auto polygon = PolygonGenerator::GeneratePolygon2(trackedObject);
        shape.polygon = polygon;
        trackedObject.shape.push_back(shape);
    }
    trackedObjects.header.frame_id = "map";
    trackedObjects.header.stamp = this->get_clock()->now();
    trackedObjects.objects.push_back(trackedObject);
    trackedObjects.objects.push_back(trackedObject2);
    pub_tracked_objects->publish(trackedObjects);

    id_tracked_obj_msg += 1;

    RCLCPP_INFO(this->get_logger(), "Tracked objects msg is published.");
}


