//
// Created by goktug on 18.05.2021.
//

#include <cstdio>
#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "TrackingVisualizerNode.h"

using namespace std::chrono_literals;

int main(int argc, char ** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<TrackingVisualizerNode>());
    rclcpp::shutdown();
    return 0;
}
