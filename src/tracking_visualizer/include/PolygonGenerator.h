//
// Created by goktug on 20.05.2021.
//

#ifndef BUILD_POLYGONGENERATOR_H
#define BUILD_POLYGONGENERATOR_H

#include "autoware_auto_msgs/msg/tracked_object.hpp"
#include "autoware_auto_msgs/msg/tracked_objects.hpp"
#include <geometry_msgs/msg/polygon.hpp>

class PolygonGenerator
{
public:
    explicit PolygonGenerator();

    static geometry_msgs::msg::Polygon
    GeneratePolygon1(const autoware_auto_msgs::msg::TrackedObject& trackedObject);

    static geometry_msgs::msg::Polygon
    GeneratePolygon2(const autoware_auto_msgs::msg::TrackedObject& trackedObject);

};


#endif //BUILD_POLYGONGENERATOR_H
