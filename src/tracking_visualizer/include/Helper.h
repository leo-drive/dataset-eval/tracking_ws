//
// Created by goktug on 18.05.2021.
//

#ifndef BUILD_HELPER_H
#define BUILD_HELPER_H

#include "rclcpp/rclcpp.hpp"

#include "autoware_auto_msgs/msg/tracked_object.hpp"
#include "autoware_auto_msgs/msg/tracked_objects.hpp"

#include <visualization_msgs/msg/marker_array.hpp>
#include <visualization_msgs/msg/marker.hpp>
#include <geometry_msgs/msg/polygon.hpp>

#include <pcl_conversions/pcl_conversions.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <builtin_interfaces/msg/time.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>

#include <iostream>
#include <functional>
#include <memory>
#include <set>
#include <cmath>

class Helper{
public:
    explicit Helper();

    void
    generateBBox(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                 visualization_msgs::msg::MarkerArray& markerArray);

    void
    generateTextObjectId(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                         visualization_msgs::msg::MarkerArray& markerArray);

    void
    generateArrowHeading(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                         visualization_msgs::msg::MarkerArray& markerArray);

    void
    generateCircleWithRadius(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                         visualization_msgs::msg::MarkerArray& markerArray, const float& radius);

    void
    visualizePolygonPoints(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                           pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_polygon_point,
                           const rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr& publisher);
    void
    generatePolygonPoints(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects,
                          visualization_msgs::msg::MarkerArray& markerArray);

    void
    setObjectIds(const autoware_auto_msgs::msg::TrackedObjects& trackedObjects);

    void
    determineObjectIdsWillBeDeleted();

    void
    updateState();

    bool
    objectMustBeDeleted(const int& id);

    std::vector<int> vector_ids_current_frame;
    std::vector<int> vector_ids_must_be_deleted;
    std::vector<int> vector_ids_previous_frame;

    //int id;
};


#endif //BUILD_HELPER_H
