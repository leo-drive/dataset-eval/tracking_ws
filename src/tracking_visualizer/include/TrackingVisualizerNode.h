//
// Created by goktug on 18.05.2021.
//

#ifndef BUILD_TRACKINGVISUALIZERNODE_H
#define BUILD_TRACKINGVISUALIZERNODE_H

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <visualization_msgs/msg/marker_array.hpp>
#include <geometry_msgs/msg/polygon.hpp>
#include <builtin_interfaces/msg/time.hpp>
#include <yaml-cpp/yaml.h>

#include <autoware_auto_msgs/msg/tracked_object.hpp>
#include <autoware_auto_msgs/msg/tracked_objects.hpp>

#include <filesystem>

#include "Helper.h"
#include "PolygonGenerator.h"

class TrackingVisualizerNode:public rclcpp::Node
{
public:
    explicit TrackingVisualizerNode();

private:
    rclcpp::Subscription<autoware_auto_msgs::msg::TrackedObjects>::SharedPtr sub_tracked_objects_;
    std::function<void(std::shared_ptr<autoware_auto_msgs::msg::TrackedObjects>)> functionSub_tracked_objects_;
    void CallbackTrackedObjects(const autoware_auto_msgs::msg::TrackedObjects::ConstSharedPtr& msg_tracked_objects);
    //rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr pub_cloud_polygon_points_;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr pub_markerArray_bbox_id_arrow_;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr pub_markerArray_cirlce_;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr pub_markerArray_polygon_points_;

    std::map<std::string, std::string> params;
    visualization_msgs::msg::MarkerArray markerArray_bbox_id_arrow;
    visualization_msgs::msg::MarkerArray markerArray_cirlce;
    visualization_msgs::msg::MarkerArray markerArray_polygon_points;
    std::shared_ptr<Helper> helper_ptr = std::make_shared<Helper>();

    // For test:
    rclcpp::TimerBase::SharedPtr timer_tracked_obj_generator_;
    void CallbackTrackedObjectGenerator();
    int id_tracked_obj_msg;
    rclcpp::Publisher<autoware_auto_msgs::msg::TrackedObjects>::SharedPtr pub_tracked_objects;
};
#endif //BUILD_TRACKINGVISUALIZERNODE_H
