from launch import LaunchDescription
import launch_ros.actions

def generate_launch_description():
    return LaunchDescription([
        launch_ros.actions.Node(
            package='tracking_visualizer',
            namespace='tracking_ws',
            executable='tracking_visualizer_node',
            output='screen'),

    ])
