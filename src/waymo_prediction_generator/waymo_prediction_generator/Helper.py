from os import listdir
from os.path import isfile, join
import numpy as np
import os
import tensorflow.compat.v1 as tf
import math
import itertools

#tf.enable_eager_execution()

from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
from sensor_msgs.msg import PointCloud2
import rclpy
import ros2_numpy as rnp
import builtin_interfaces.msg


class WaymoPredictionGenerator(object):
    def __init__(self, path_tfrecords):
        self.path_tfrecords = path_tfrecords
        self.list_path_tfrecords = [f for f in listdir(path_tfrecords) if isfile(join(path_tfrecords, f))]
        self.record_count = len(self.list_path_tfrecords)

        self.id_current_dataset = 0
        self.id_current_frame = 0
        self.frame_count_current_dataset = 0
        self.list_msg_point_cloud = []
        self.path_current_tfrecord = self.path_tfrecords + '/' + self.list_path_tfrecords[self.id_current_dataset]

        self.list_timestamp_waymo = []
        self.list_timestamp_ros = []
        self.context_name_current = ""
        self.lidar_labels_current = []

        self.UpdateSegment()

    def UpdateSegment(self):
        print("*******************************************************************************************************")
        print("Please wait segment is reading...")
        self.id_current_frame = 0
        self.list_msg_point_cloud = []
        self.path_current_tfrecord = self.path_tfrecords + '/' + self.list_path_tfrecords[self.id_current_dataset]

        dataset = tf.data.TFRecordDataset(self.path_current_tfrecord, compression_type='')

        frame_counter = 0

        self.list_timestamp_waymo = []
        self.list_timestamp_ros = []
        self.lidar_labels_current = []

        for frame_id, data in enumerate(dataset):
            #print("Frame ", frame_id, "is read.")
            frame_counter = frame_id

            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))
            self.lidar_labels_current.append(frame.laser_labels)

            self.context_name_current = frame.context.name

            self.list_timestamp_waymo.append(frame.timestamp_micros)

            frame_stamp_secs = int(frame.timestamp_micros / 1000000)
            frame_stamp_nsecs = int((frame.timestamp_micros / 1000000.0 - frame_stamp_secs) * 1000000000)
            time_stamp_ros = (frame_stamp_secs, frame_stamp_nsecs)
            self.list_timestamp_ros.append(time_stamp_ros)

            (range_images, camera_projections, range_image_top_pose) = self.parse_range_image_and_camera_projection(
                frame)
            points, cp_points = self.convert_range_image_to_point_cloud(frame, range_images, camera_projections,
                                                                        range_image_top_pose)

            x_values = []
            y_values = []
            z_values = []

            for index, lidar_points in enumerate(points):

                for point in points[index]:
                    x_values.append(point[0])
                    y_values.append(point[1])
                    z_values.append(point[2])

            point_cloud = np.ones(len(x_values), dtype=[
                ('x', np.float32),
                ('y', np.float32),
                ('z', np.float32),
            ])

            point_cloud['x'] = np.array(x_values, np.float32)
            point_cloud['y'] = np.array(y_values)
            point_cloud['z'] = np.array(z_values)

            msg_point_cloud = rnp.msgify(PointCloud2, point_cloud)
            msg_point_cloud.header.frame_id = 'vehicle'
            msg_point_cloud.height = 1

            timestamp = builtin_interfaces.msg.Time()
            timestamp.sec = frame_stamp_secs
            timestamp.nanosec = frame_stamp_nsecs
            msg_point_cloud.header.stamp = timestamp

            self.list_msg_point_cloud.append(msg_point_cloud)

        self.frame_count_current_dataset = frame_counter
        self.id_current_dataset += 1

        print("Frame count: ", self.frame_count_current_dataset)
        print("Segment is reading is done.")

    def parse_range_image_and_camera_projection(self, frame):
        range_images = {}
        camera_projections = {}
        range_image_top_pose = None
        for laser in frame.lasers:
            if len(laser.ri_return1.range_image_compressed) > 0:
                range_image_str_tensor = tf.io.decode_compressed(laser.ri_return1.range_image_compressed, 'ZLIB')
                ri = open_dataset.MatrixFloat()
                ri.ParseFromString(bytearray(range_image_str_tensor.numpy()))
                range_images[laser.name] = [ri]
                if laser.name == open_dataset.LaserName.TOP:
                    range_image_top_pose_str_tensor = tf.io.decode_compressed(
                        laser.ri_return1.range_image_pose_compressed, 'ZLIB')
                    range_image_top_pose = open_dataset.MatrixFloat()
                    range_image_top_pose.ParseFromString(bytearray(range_image_top_pose_str_tensor.numpy()))
                camera_projection_str_tensor = tf.io.decode_compressed(laser.ri_return1.camera_projection_compressed,
                                                                       'ZLIB')
                cp = open_dataset.MatrixInt32()
                cp.ParseFromString(bytearray(camera_projection_str_tensor.numpy()))
                camera_projections[laser.name] = [cp]
            if len(laser.ri_return2.range_image_compressed) > 0:
                range_image_str_tensor = tf.io.decode_compressed(laser.ri_return2.range_image_compressed, 'ZLIB')
                ri = open_dataset.MatrixFloat()
                ri.ParseFromString(bytearray(range_image_str_tensor.numpy()))
                range_images[laser.name].append(ri)
                camera_projection_str_tensor = tf.io.decode_compressed(laser.ri_return2.camera_projection_compressed,
                                                                       'ZLIB')
                cp = open_dataset.MatrixInt32()
                cp.ParseFromString(bytearray(camera_projection_str_tensor.numpy()))
                camera_projections[laser.name].append(cp)
        return range_images, camera_projections, range_image_top_pose

    def convert_range_image_to_point_cloud(self, frame, range_images, camera_projections, range_image_top_pose,
                                           ri_index=0):
        calibrations = sorted(frame.context.laser_calibrations, key=lambda c: c.name)
        lasers = sorted(frame.lasers, key=lambda laser: laser.name)
        points = []
        cp_points = []
        frame_pose = tf.convert_to_tensor(np.reshape(np.array(frame.pose.transform), [4, 4]))
        # [H, W, 6]
        range_image_top_pose_tensor = tf.reshape(tf.convert_to_tensor(range_image_top_pose.data),
                                                 range_image_top_pose.shape.dims)
        # [H, W, 3, 3]
        range_image_top_pose_tensor_rotation = transform_utils.get_rotation_matrix(range_image_top_pose_tensor[..., 0],
                                                                                   range_image_top_pose_tensor[..., 1],
                                                                                   range_image_top_pose_tensor[..., 2])
        range_image_top_pose_tensor_translation = range_image_top_pose_tensor[..., 3:]
        range_image_top_pose_tensor = transform_utils.get_transform(range_image_top_pose_tensor_rotation,
                                                                    range_image_top_pose_tensor_translation)
        for c in calibrations:
            range_image = range_images[c.name][ri_index]
            if len(c.beam_inclinations) == 0:
                beam_inclinations = range_image_utils.compute_inclination(
                    tf.constant([c.beam_inclination_min, c.beam_inclination_max]), height=range_image.shape.dims[0])
            else:
                beam_inclinations = tf.constant(c.beam_inclinations)
            beam_inclinations = tf.reverse(beam_inclinations, axis=[-1])
            extrinsic = np.reshape(np.array(c.extrinsic.transform), [4, 4])
            range_image_tensor = tf.reshape(tf.convert_to_tensor(range_image.data), range_image.shape.dims)
            pixel_pose_local = None
            frame_pose_local = None
            if c.name == open_dataset.LaserName.TOP:
                pixel_pose_local = range_image_top_pose_tensor
                pixel_pose_local = tf.expand_dims(pixel_pose_local, axis=0)
                frame_pose_local = tf.expand_dims(frame_pose, axis=0)
            range_image_mask = range_image_tensor[..., 0] > 0
            range_image_cartesian = range_image_utils.extract_point_cloud_from_range_image(
                tf.expand_dims(range_image_tensor[..., 0], axis=0), tf.expand_dims(extrinsic, axis=0),
                tf.expand_dims(tf.convert_to_tensor(beam_inclinations), axis=0), pixel_pose=pixel_pose_local,
                frame_pose=frame_pose_local)
            range_image_cartesian = tf.squeeze(range_image_cartesian, axis=0)
            points_tensor = tf.gather_nd(range_image_cartesian, tf.where(range_image_mask))
            cp = camera_projections[c.name][0]
            cp_tensor = tf.reshape(tf.convert_to_tensor(cp.data), cp.shape.dims)
            cp_points_tensor = tf.gather_nd(cp_tensor, tf.where(range_image_mask))
            points.append(points_tensor.numpy())
            cp_points.append(cp_points_tensor.numpy())
        return points, cp_points

    def GivePointCloud(self):
        return self.list_msg_point_cloud[self.id_current_frame]

    def PublishPointCloud(self, publisher, msg_point_cloud):
        publisher.publish(msg_point_cloud)
        print("Point cloud message is published.")

    def GetDatasetTimeStamp(self, secs, nsecs):
        for i in range(self.frame_count_current_dataset):
            (ros_secs,ros_nsecs) = self.list_timestamp_ros[i]
            if(ros_secs==secs and ros_nsecs == nsecs):
                #print("Dataset time: ", self.list_timestamp_waymo[i],"(microseconds)")
                return i, self.list_timestamp_waymo[i]

    def giveLidarLabels(self, frame_id):
        return self.lidar_labels_current[frame_id]

    def getClosestObject(self, lidar_labels, tracked_object_prediction):

        closest_id = 0
        distances = []

        for lidar_label in lidar_labels:
            x = lidar_label.box.center_x
            y = lidar_label.box.center_y
            z = lidar_label.box.center_z

            px = tracked_object_prediction.bbox.center.position.x
            py = tracked_object_prediction.bbox.center.position.y
            pz = tracked_object_prediction.bbox.center.position.z

            dist = math.sqrt(
                pow(x-px,2)+pow(y-py,2)+pow(z-pz,2)
            )
            distances.append(dist)

        closest_id = np.argmin(distances)
        return (lidar_labels[closest_id].box.center_x,
                lidar_labels[closest_id].box.center_y,
                lidar_labels[closest_id].box.center_z,
                lidar_labels[closest_id].box.length,
                lidar_labels[closest_id].box.width,
                lidar_labels[closest_id].box.height,
                lidar_labels[closest_id].box.heading)



