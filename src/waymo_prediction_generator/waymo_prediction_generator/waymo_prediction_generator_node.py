import rclpy
from rclpy.node import Node
import numpy as np
import autoware_auto_msgs.msg as autoware_auto_msgs
import sys
from Helper import WaymoPredictionGenerator
from sensor_msgs.msg import PointCloud2
from std_msgs.msg import Int8
from waymo_open_dataset import label_pb2
from waymo_open_dataset.protos import metrics_pb2
import time

from vision_msgs.msg import Detection3DArray
from vision_msgs.msg import ObjectHypothesisWithPose
from vision_msgs.msg import Detection2D


class WaymoPredictionGeneratorNode(Node):
    def __init__(self):
        super().__init__('waymo_prediction_node')

        self.prediction_generator = WaymoPredictionGenerator(sys.argv[1])
        self.publisher_frequency = 10

        # Proto objects all:
        self.proto_objects_calculated = metrics_pb2.Objects()

        # Publish Waymo PointClouds
        self.publisher = self.create_publisher(PointCloud2, '/point_cloud_waymo', 1)
        self.timer = self.create_timer(1 / self.publisher_frequency, self.CallbackPublishPointCloud)
        self.publish_point_cloud_is_done = False

        # Publish state reset flag:
        self.pub_flag = self.create_publisher(Int8, '/flag_reset_tracker', 1)

        # Subscribe tracked objects:
        self.write_pred_file = True
        self.subscription = self.create_subscription(Detection3DArray, '/tracked_objects', self.CallbackTrackedObject, 10)

    def CallbackPublishPointCloud(self):

        if self.prediction_generator.id_current_frame == self.prediction_generator.frame_count_current_dataset:
            if self.prediction_generator.record_count == self.prediction_generator.id_current_dataset:
                print("All point clouds are published.")
                self.publish_point_cloud_is_done = True
                time.sleep(5)  # Sleep for 5 seconds

                if self.write_pred_file:
                    output_path = str(sys.argv[1]) + '/' + 'my_prediction_file.bin'
                    f = open(output_path, 'wb')
                    f.write(self.proto_objects_calculated.SerializeToString())
                    f.close()
                    self.write_pred_file = False
            else:
                msg_reset_state = Int8()
                #msg_reset_state = 1
                self.pub_flag.publish(msg_reset_state)
                self.prediction_generator.UpdateSegment()

        if not self.publish_point_cloud_is_done:
            print("Current frame id:", self.prediction_generator.id_current_frame)
            point_cloud = self.prediction_generator.GivePointCloud()
            self.prediction_generator.PublishPointCloud(self.publisher, point_cloud)
            self.prediction_generator.id_current_frame += 1

    def CallbackTrackedObject(self, msg):
        print("Message is received. | Id: ", "| ROS2 time:", msg.header.stamp.sec, "(sec)",msg.header.stamp.nanosec, "(nanosec)")

        id_frame, time_stamp_waymo = self.prediction_generator.GetDatasetTimeStamp(msg.header.stamp.sec,
                                                                                   msg.header.stamp.nanosec)

        print("Waymo dataset frame id: ", id_frame)
        print("Waymo time stamp: ", time_stamp_waymo)
        print("Tracked object count: ", len(msg.detections))

        frame_lidar_labels = self.prediction_generator.giveLidarLabels(id_frame)

        for tracked_object_prediction in msg.detections:
            object_calculated = metrics_pb2.Object()
            object_calculated.context_name = self.prediction_generator.context_name_current
            object_calculated.frame_timestamp_micros = time_stamp_waymo

            box = label_pb2.Label.Box()
            box.center_x = tracked_object_prediction.bbox.center.position.x
            box.center_y = tracked_object_prediction.bbox.center.position.y
            box.center_z = tracked_object_prediction.bbox.center.position.z
            box.length = tracked_object_prediction.bbox.size.x
            box.width = tracked_object_prediction.bbox.size.y
            box.height = tracked_object_prediction.bbox.size.z
            box.heading = 0
            object_calculated.object.box.CopyFrom(box)

            object_calculated.score = 1.0

            object_calculated.object.id = tracked_object_prediction.results[0].id

            object_calculated.object.type = 1

            self.proto_objects_calculated.objects.append(object_calculated)

        # Giving ground truth:
        '''frame_lidar_labels = self.prediction_generator.giveLidarLabels(id_frame)
        for tracked_object in frame_lidar_labels:
            object_calculated = metrics_pb2.Object()

            object_calculated.context_name = self.prediction_generator.context_name_current
            object_calculated.frame_timestamp_micros = time_stamp_waymo

            box = label_pb2.Label.Box()
            box.center_x = tracked_object.box.center_x
            box.center_y = tracked_object.box.center_y
            box.center_z = tracked_object.box.center_z
            box.length = tracked_object.box.length
            box.width = tracked_object.box.width
            box.height = tracked_object.box.height
            box.heading = tracked_object.box.heading
            object_calculated.object.box.CopyFrom(box)

            object_calculated.score = 1.0

            object_calculated.object.id = tracked_object.id

            object_calculated.object.type = tracked_object.type

            self.proto_objects_calculated.objects.append(object_calculated)'''


def main(args=None):

    rclpy.init(args=args)
    node = WaymoPredictionGeneratorNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
