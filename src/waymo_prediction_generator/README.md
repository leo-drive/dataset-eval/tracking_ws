## How to install
### Requirements:
You should install and build Waymo Open Datset from source.
```bash
https://github.com/Box-Robotics/ros2_numpy
https://github.com/waymo-research/waymo-open-dataset
```
Before building the Waymo Open Dataset from source, replace /waymo_open_dataset/metrics/tools/compute_tracking_metrics_main.cc with it which we provide.

You can use those instructions to build:
```bash
https://github.com/waymo-research/waymo-open-dataset/blob/master/tutorial/tutorial.ipynb
```
Also, recommended TensorFlow version is 2.1.0 to build Waymo Open Dataset from source. 
## How to use
Download Waymo validation set from:
```bash
https://console.cloud.google.com/storage/browser/waymo_open_dataset_v_1_2_0
```
Don't forget download ground truth file: gt.bin
Populate a folder with the .tfrecord files from Waymo validation set. Then,
```bash
python3 waymo_prediction_generator_node.py /home/goktug/Desktop/Waymo_Valid_Dataset/tf_record
```
After prediction generator is started, it reads .tfrecord files and publishes point clouds in order using '/point_cloud_waymo' topic.

You should publish your tracked objects using '/tracked_objects' (Detection3DArray) topic. 

Once the whole point clouds are published in a .tfrecord file, Int8 message will be published using '/flag_reset_tracker' topic. It aims to reset the state of your estimator. Then new .tfrecord file will be read.

The whole process ends up with 'my_prediction_file.bin' file into the tfrecord folder.

To evaluate,
```bash
./compute_tracking_metrics_main /home/goktug/Desktop/Waymo_Valid_Dataset/tf_record/my_prediction_file.bin /gt.bin
```
Note that: Change IoU ratio in compute_tracking_metrics_main.cc, as what you want in order to avoid all metrics are zero! IoU order is like this:

https://github.com/waymo-research/waymo-open-dataset/blob/master/waymo_open_dataset/label.proto#L60