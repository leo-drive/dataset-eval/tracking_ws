import math
import numpy as np

# Waymo
import tensorflow.compat.v1 as tf
tf.enable_eager_execution()
from waymo_open_dataset import dataset_pb2 as open_dataset
import motmetrics as mm


class CustomTrackingEvaluator(object):
    def __init__(self, path_tf_record, association_threshold):
        print("TFRecord File: ", path_tf_record)
        self.list_timestamp_waymo = []
        self.list_timestamp_ros = []
        self.list_lidar_labels = []
        self.frame_count = 0
        self.ReadDataset(path_tf_record)

        # Waymo Open Dataset Object Filter Default Params:
        self.vehicle = True
        self.pedestrian = True
        self.sign = True
        self.cyclist = True
        self.range_x = (-30, 30)
        self.range_y = (-30, 30)
        self.range_z = (-30, 30)

        # Create an accumulator that will be updated during each frame
        self.acc = mm.MOTAccumulator(auto_id=True)
        self.association_threshold = association_threshold

        self.list_filtered_hypotheses = []
        self.list_filtered_hypotheses_ids = []

        self.list_filtered_waymo_objects = []

    def ReadDataset(self, file):
        dataset = tf.data.TFRecordDataset(file, compression_type='')
        for data in dataset:
            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))
            self.list_timestamp_waymo.append(frame.timestamp_micros)
            time_stamp_ros_secs = int(frame.timestamp_micros / 1000000)
            time_stamp_ros_nsec = int((frame.timestamp_micros / 1000000.0 - time_stamp_ros_secs) * 1000000000)
            time_stamp_ros = (time_stamp_ros_secs, time_stamp_ros_nsec)
            self.list_timestamp_ros.append(time_stamp_ros)
            self.list_lidar_labels.append(frame.laser_labels)
        self.frame_count = len(self.list_timestamp_waymo)
        print("Dataset is read. Frame count: ", self.frame_count)

    def GetDatasetTimeStamp(self, secs, nsecs):
        for i in range(self.frame_count):
            (ros_secs, ros_nsecs) = self.list_timestamp_ros[i]
            if (ros_secs == secs and ros_nsecs == nsecs):
                return i, self.list_timestamp_waymo[i]

    def giveWaymoLidarLabels(self, frame_id):
        return self.list_lidar_labels[frame_id]

    def setFilterConfig(self, vehicle, pedestrian, sign, cyclist, range_x, range_y, range_z):
        self.vehicle = vehicle
        self.pedestrian = pedestrian
        self.sign = sign
        self.cyclist = cyclist
        self.range_x = range_x
        self.range_y = range_y
        self.range_z = range_z

    def FilterOutWaymoObjects(self):
        ids = []
        for frame_labels in self.list_lidar_labels:
            for object_label in frame_labels:
                ids.append(object_label.id)
        print("Total Waymo object count before filtering: ", len(list(set(ids))))

        list_out_ids = []
        for frame_labels in self.list_lidar_labels:
            for object_label in frame_labels:
                condition_type = (object_label.type == 1 and self.vehicle == True) or \
                                 (object_label.type == 2 and self.pedestrian == True) or \
                                 (object_label.type == 3 and self.sign == True) or \
                                 (object_label.type == 4 and self.cyclist == True)
                condition_range = (self.range_x[0] <= object_label.box.center_x <= self.range_x[1]) and \
                                  (self.range_y[0] <= object_label.box.center_y <= self.range_y[1]) and \
                                  (self.range_z[0] <= object_label.box.center_z <= self.range_z[1])
                if condition_type and condition_range:
                    pass
                else:
                    list_out_ids.append(object_label.id)
        list_out_ids = list(set(list_out_ids))
        print("Total filtered object count: ", len(list_out_ids))

        list_waymo_objects = []
        ids_check = []
        for frame_labels in self.list_lidar_labels:
            list_filtered_frame_labels = []
            for object_label in frame_labels:
                if object_label.id not in list_out_ids:
                    list_filtered_frame_labels.append(object_label)
                else:
                    ids_check.append(object_label.id)
            list_waymo_objects.append(list_filtered_frame_labels)
        assert len(list(set(ids_check))) == len(list_out_ids)
        assert len(list(set(ids))) != len(
            list_out_ids), "There is no Waymo object to evaluate tracker! Change filter params."

        self.list_lidar_labels = list_waymo_objects

    def Cost(self, object, hypothesis):
        dist = math.sqrt(
            pow(object.box.center_x - hypothesis.kinematics.pose.pose.position.x, 2) +
            pow(object.box.center_y - hypothesis.kinematics.pose.pose.position.y, 2)
        )
        if dist < self.association_threshold:
            return dist
        else:
            return np.nan

    def FilterOutHypothesis(self, hypothesis):
        condition_range = (self.range_x[0] <= hypothesis.kinematics.pose.pose.position.x <= self.range_x[1]) and \
                          (self.range_y[0] <= hypothesis.kinematics.pose.pose.position.y <= self.range_y[1]) and \
                          (self.range_z[0] <= hypothesis.kinematics.pose.pose.position.z <= self.range_z[1])
        return condition_range

    def UpdateAccumulator(self, waymo_frame_lidar_labels, msg_tracked_objects):
        # print("Object count: ", len(waymo_frame_lidar_labels))
        # print("Hypothesis count:", len(msg_tracked_objects.objects))

        list_frame_filtered_hyp = []
        # Iterate over the hypotheses:
        for hypothesis in msg_tracked_objects.objects:
            if self.FilterOutHypothesis(hypothesis):
                list_frame_filtered_hyp.append(hypothesis)
            else:
                self.list_filtered_hypotheses_ids.append(hypothesis.object_id)

        self.list_filtered_hypotheses.append(list_frame_filtered_hyp)
        self.list_filtered_waymo_objects.append(waymo_frame_lidar_labels)

    def calculateMetrics(self):

        # The set of filtered hypotheses ids
        self.list_filtered_hypotheses_ids = list(set(self.list_filtered_hypotheses_ids))

        # Iterate over the whole frames:
        for frame_id in range(len(self.list_filtered_hypotheses)):
            waymo_frame_lidar_labels = self.list_filtered_waymo_objects[frame_id]
            frame_hypotheses = self.list_filtered_hypotheses[frame_id]

            list_object_ids = []
            list_hypothesis_ids = []
            list_distances = []

            # Iterate over the ground truth objects:
            for id, object in enumerate(waymo_frame_lidar_labels):
                
                object_id = codecs.encode(object.id[len(object.id) - 8:len(object.id)])
                object_id = int.from_bytes(object_id, byteorder='big', signed=False)
                object_id = int(str(object_id)[:5])
                
                list_object_ids.append(object_id)
                list_distances_to_single_object = []

                # Iterate over the hypotheses:
                for hypothesis in frame_hypotheses:
                    if hypothesis.object_id in self.list_filtered_hypotheses_ids:
                        continue
                    list_distances_to_single_object.append(self.Cost(object, hypothesis))
                    if id == 0:
                        list_hypothesis_ids.append(hypothesis.object_id)

                list_distances.append(list_distances_to_single_object)

            self.acc.update(list_object_ids, list_hypothesis_ids, list_distances)

            # Frame summary:
            print("\nSummary of the frame: ")
            print(self.acc.mot_events.loc[frame_id], '\n')

        mh = mm.metrics.create()
        summary = mh.compute(self.acc,
                             metrics=['num_frames', 'mota', 'motp', 'num_matches', 'num_misses', 'num_false_positives',
                                      'num_switches'], name='acc')
        print("MOTA Interval: [-1,1]")
        print("MOTP Interval: [0,inf)")
        print("\n\n", summary)
        print("\n\nFiltered hypothesis due to range filter:", self.list_filtered_hypotheses_ids)


class WaymoPredictionGenerator(object):
    def __init__(self):
        pass
