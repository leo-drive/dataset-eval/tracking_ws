import rclpy
from rclpy.node import Node
import numpy as np
import autoware_auto_msgs.msg as autoware_auto_msgs
import visualization_msgs.msg as visualization_msgs
from Helper import CustomTrackingEvaluator
import motmetrics as mm
import sys


class TrackingEvaluatorNode(Node):
    def __init__(self):
        super().__init__('tracking_evaluator_node')
        # Input parameters **************************:
        self.path_tf_record = "/home/goktug/Desktop/Waymo_Valid_Dataset/tf_record/segment-10335539493577748957_1372_870_1392_870_with_camera_labels.tfrecord"
        self.association_threshold = 1.5
        self.frame_id_show_result = 197
        # Filter configs:
        vehicle = True
        pedestrian = True
        sign = True
        cyclist = True
        # Ranges in vehicle(base_link) frame:
        # If any ground truth object is in those intervals at any time, It'll be filtered.
        range_x = (-100, 300)  # -30, 40
        range_y = (-10, 10)  # -3, 10
        range_z = (-1000, 1000)

        # Input parameters END ******************************************************************
        self.eval = CustomTrackingEvaluator(self.path_tf_record, self.association_threshold)
        self.eval.setFilterConfig(vehicle, pedestrian, sign, cyclist, range_x, range_y, range_z)
        self.eval.FilterOutWaymoObjects()

        self.subscription = self.create_subscription(autoware_auto_msgs.TrackedObjects, '/tracked_objects',
                                                     self.CallbackTrackedObjects, 10)

    def CallbackTrackedObjects(self, msg_tracked_objects):
        print("Message is received. | ROS2 time:", msg_tracked_objects.header.stamp.sec, "(sec)",
              msg_tracked_objects.header.stamp.nanosec, "(nanosec)")

        id_frame, time_stamp_waymo = self.eval.GetDatasetTimeStamp(msg_tracked_objects.header.stamp.sec,
                                                                   msg_tracked_objects.header.stamp.nanosec)
        waymo_frame_lidar_labels = self.eval.giveWaymoLidarLabels(id_frame)
        self.eval.UpdateAccumulator(waymo_frame_lidar_labels, msg_tracked_objects)

        print("Dataset frame id: ", id_frame)

        if (id_frame == self.frame_id_show_result):
            self.eval.calculateMetrics()

        print("************************************************************************")


def main(args=None):
    rclpy.init(args=args)
    node = TrackingEvaluatorNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
