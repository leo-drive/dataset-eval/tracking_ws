import os
import tensorflow.compat.v1 as tf
import math
import numpy as np
import itertools
from waymo_open_dataset import dataset_pb2
from waymo_open_dataset import label_pb2
from waymo_open_dataset.protos import metrics_pb2

tf.enable_eager_execution()
from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
import random


proto_objects_calculated = metrics_pb2.Objects()

path_tf_record = '/home/goktug/Desktop/Waymo_Valid_Dataset/tf_record/' \
                 'segment-10335539493577748957_1372_870_1392_870_with_camera_labels.tfrecord'

dataset_gt = tf.data.TFRecordDataset(path_tf_record, compression_type='')
counter_frame = 0


for data_gt in dataset_gt:
    # Each frame
    frame_gt = open_dataset.Frame()
    frame_gt.ParseFromString(bytearray(data_gt.numpy()))

    frame_labels_gt = frame_gt.laser_labels
    frame_context_name = frame_gt.context.name

    counter_object = 0
    for object_gt in frame_labels_gt:
        # print(object_ground_truth)
        # Each object within a frame
        object_calculated = metrics_pb2.Object()

        object_calculated.context_name = frame_context_name
        object_calculated.frame_timestamp_micros = frame_gt.timestamp_micros

        box = label_pb2.Label.Box()
        box.center_x = object_gt.box.center_x
        box.center_y = object_gt.box.center_y
        box.center_z = object_gt.box.center_z
        box.length = object_gt.box.length
        box.width = object_gt.box.width
        box.height = object_gt.box.height
        box.heading = object_gt.box.heading
        object_calculated.object.box.CopyFrom(box)

        object_calculated.score = 1.0

        object_calculated.object.id = object_gt.id

        object_calculated.object.type = object_gt.type

        # print('fr: ' + str(counter_frame) + ', obj: ' + str(counter_object) + ' id = ' + str(o.object.id))

        proto_objects_calculated.objects.append(object_calculated)
        counter_object += 1
    counter_frame += 1


path_tf_record = '/home/goktug/Desktop/Waymo_Valid_Dataset/tf_record/' \
                 "segment-10359308928573410754_720_000_740_000_with_camera_labels.tfrecord"

dataset_gt = tf.data.TFRecordDataset(path_tf_record, compression_type='')
counter_frame = 0


for data_gt in dataset_gt:
    # Each frame
    frame_gt = open_dataset.Frame()
    frame_gt.ParseFromString(bytearray(data_gt.numpy()))

    frame_labels_gt = frame_gt.laser_labels
    frame_context_name = frame_gt.context.name

    counter_object = 0
    for object_gt in frame_labels_gt:
        # print(object_ground_truth)
        # Each object within a frame
        object_calculated = metrics_pb2.Object()

        object_calculated.context_name = frame_context_name
        object_calculated.frame_timestamp_micros = frame_gt.timestamp_micros

        box = label_pb2.Label.Box()
        box.center_x = object_gt.box.center_x
        box.center_y = object_gt.box.center_y
        box.center_z = object_gt.box.center_z
        box.length = object_gt.box.length
        box.width = object_gt.box.width
        box.height = object_gt.box.height
        box.heading = object_gt.box.heading
        object_calculated.object.box.CopyFrom(box)

        object_calculated.score = 1.0

        object_calculated.object.id = object_gt.id

        object_calculated.object.type = object_gt.type

        # print('fr: ' + str(counter_frame) + ', obj: ' + str(counter_object) + ' id = ' + str(o.object.id))

        proto_objects_calculated.objects.append(object_calculated)
        counter_object += 1
    counter_frame += 1

f = open('/home/goktug/Desktop/my_prediction_file.bin', 'wb')
f.write(proto_objects_calculated.SerializeToString())
f.close()

